<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

// composer autoloader required for AMQP libraries
require_once('config.inc.php');
require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );

$servername = $dbconfig['db_server'];
$username = $dbconfig['db_username'];
$password = $dbconfig['db_password'];
$dbname = $dbconfig['db_name'];
$homepath = '/var/www/vtigerdev/';

//Batelco Batch Details


function Batelco_Batch_Scheduler() {
 
	function get_db_connection($db_sql){
		
		// Create connection
                $conn = new mysqli($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                doLog($db_sql);
                $db_result = $conn->query($db_sql);
		$conn->close();
		return $db_result;

	}

	// Prepare data from vtiger sales order
	function get_data_from_db(){

		$mod_sdate = date("Y-m-d");
                $mod_edate = date("Y-m-d",strtotime("+1 days"));
                doLog($mod_sdate);
                doLog($mod_edate);

		$bat_sql = "select s.salesorderid, s.salesorder_no, potcf.cf_954, potcf.cf_956, acc.accountid, acc.accountname, acccf.cf_827, crm.modifiedtime from vtiger_salesorder s, vtiger_salesordercf scf, vtiger_account acc, vtiger_accountscf acccf, vtiger_potentialscf potcf, vtiger_crmentity crm where s.salesorderid = scf.salesorderid and s.potentialid = potcf.potentialid and s.accountid = acc.accountid and s.salesorderid = crm.crmid and acc.accountid = acccf.accountid and s.sostatus = 'Approved' and scf.cf_884='Bitstream' and potcf.cf_954 != '' and potcf.cf_956 !='' and crm.modifiedtime >= '".$mod_sdate."' and crm.modifiedtime < '".$mod_edate."';";
		$result = get_db_connection($bat_sql);
		$r_result = get_db_connection($bat_sql);
		$row_cnt = $result->num_rows;

		// Just get the current date
        	$startdate = date("d.m.Y");
        	$targetdate = date("d.m.Y",strtotime("+44 days"));

		if ($row_cnt > 0){

        	$htmlFile = '<html>
                     <head>
                     </head>
                    <body>
                    <br />
                    <div>
                        The Team<br />
                        SMU <br />
                        Batelco <br />
                        P.O. Box 14 <br />
                        Kingdom Of Bahrain <br />
                        <br /> <br />
                        ' .$startdate. '
                        <br /> <br />
                        Re: BTS Transfer Request Batch No. B-BTS-' .$startdate. '-' .$row_cnt. '
                        <br /> <br />
                        Dear Ali/Noaf,
                        <br /> <br />
                        Please find enclosed Order Consulting Batch No. <br /> 
                        B-BTS-' .$startdate. '-01 for the following Circuit Numbers / Account name:
                        <br /> <br />
                    </div>
                    <div>';
		
		if ($result->num_rows > 0) {
		    $htmlFile .= '<table style="border-collapse: collapse; width: 100%;">
                     <tbody>
                      <tr>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">No</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Customer Name</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Circuit Number</th>
                      </tr>';
		    // output data of each row
		    $count = 0;
		    while($row = $result->fetch_assoc()) {
			$count += 1;
			$htmlFile .= '<tr>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$count.'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["accountname"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_954"].'</td>
                      </tr>';
		    }
		    $htmlFile .= '</tbody>
                    </table>';
		 } else {
                    $htmlFile .='0 results';
                }

		$htmlFile .='</div>
                <br />
                <div>
		    <br />
                    Please do not hesitate to contact me if you require any clarifications.
                    <br /><br />
                    <br /><br />
                    Prepared and Checked by <br/>
                    <br /><br />
                    <br /><br />
                    Charles Christopher
                    <br /><br />
                    <br /><br />
                </div>
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
		<br /> <br />
                <br /> <br />';
	
		if ($result->num_rows > 0) {
                    $htmlFile .= '<table style="border-collapse: collapse; width: 100%;">
                     <tbody>
                      <tr>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">No</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Customer Name</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Circuit Number</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">User Id</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">CPR/CR</th>
              <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Batelco Comments</th>
                      </tr>';
                    // output data of each row
                    $r_count = 0;
                    while($row = $r_result->fetch_assoc()) {
                        $r_count += 1;
                        $htmlFile .= '<tr>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$r_count.'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["accountname"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_954"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_956"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_827"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;"></td>
                      </tr>';
                    }
                    $htmlFile .= '</tbody>
                    </table>';
                 } else {
                    $htmlFile .='0 results';
                }

                $htmlFile .= '</body>
                </html>';

		return array($htmlFile,$row_cnt,$startdate);
		}
		else{
			return null;
		}
	}

	$data = get_data_from_db();	
	
        doLog("Batelco Batch Creation");
        //doLog($data);

	function send_mail_to_batelco($pdf_resp){
	
		$mail_subject = 'Batelco Batch File';
		$mail_content = 'Please find enclosed the Bitstream batch ' .$pdf_resp['batch_file_name']. ' for your perusal.';
		$pdf_out = $pdf_resp['output_file'];
		$file_array = Array('file_name'=>$pdf_out, 'attachment_ids'=>Array());

		$mod_sdate = date("Y-m-d");
                $mod_edate = date("Y-m-d",strtotime("+1 days"));
                doLog($mod_sdate);
                doLog($mod_edate);
		$att_sql = 'select att.crmid from vtiger_seattachmentsrel att, vtiger_crmentity crm where att.crmid = crm.crmid and crm.label = "Signed Bitstream Order Form" and crm.deleted = 0 and crm.modifiedtime >= "'.$mod_sdate.'" and crm.modifiedtime < "'.$mod_edate.'";';
                $att_result = get_db_connection($att_sql);
		doLog($att_result);

                while($row = $att_result->fetch_assoc()) {
			doLog($row);
			array_push($file_array['attachment_ids'], $row['crmid']);
		}
		$typ = gettype($file_array);
		doLog($file_array);
		doLog($typ);
		
		$cc_emails = Array();
		$user_sql = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = 'H16';";
                $user_data = get_db_connection($user_sql);
		while($field = $user_data->fetch_assoc()) {
                    array_push($cc_emails, $field['email1']);
                }
		$comma_separated_emailids = implode(",", $cc_emails);
		doLog($comma_separated_emailids);

		$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['attach'], 'abrahambinny@gmail.com', $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $pdf_resp['htmlFile'], $comma_separated_emailids,'', $file_array);

	}


	function html_to_pdf($data){
		
		$htmlFile = $data[0];
        	$row_count = $data[1];
        	$date = $data[2];

		$batch_file_name = 'B-BTS-' .$date. '-' .$row_count.'.pdf';
		$output_file = 'batchfiles/'.$batch_file_name;
		$output_file_path = 'test/upload/'.$output_file;
		$html_file_path = 'test/upload/batchfiles/batch_mail.html';
		
		/*
		$file_path =  'html2pdf/html2fpdf.php';
		require_once($file_path);
		$myPDF = new HTML2FPDF();
		$myPDF->HTML2FPDF("P","mm","A4");
		$myPDF->AddPage();
		$myPDF->WriteHTML($htmlFile);
		$myPDF->Output($output_file_path, 'F');
		*/

		$html = fopen($html_file_path, "w+");
		fwrite($html, $htmlFile);
		$shell_cmd = "wkhtmltopdf '".$html_file_path."' '".$output_file_path."'";
		doLog($shell_cmd);
		shell_exec($shell_cmd);
        	doLog("Batelco Batch Created Sucessfully");
		return Array('output_file' => $output_file, 'batch_file_name' => $batch_file_name, 'htmlFile'=> $htmlFile);
				
	}

	if($data!=null){
		$pdf_resp = html_to_pdf($data);
        	doLog($pdf_resp);
		send_mail_to_batelco($pdf_resp);
	}
        doLog("Exit Batelco Batch Sceduler Function...");
}

Batelco_Batch_Scheduler();
?>
