<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );

function Send_Mail_Notification($entity) 
{

	doLog("---------- Start Process - Send_Mail_Notification ----------");       
	global $adb;

	function send_notification($entity, $adb){
		/*
		 Send mail notifications to different users from Project Tasks
		*/
        	$entityData = $entity->data;
        	$entity_id = str_replace("31x","",$entityData['id']);

		$t_status = $entityData['projecttaskstatus'];
		$t_no = $entityData['projecttask_no'];
		$t_task_for = $entityData['cf_876'];
		$circ_id = $entityData['cf_980'];
		$sub_id = $entityData['cf_982'];
		$project_id = explode('x', $entityData['projectid']);
                $proj_id = $project_id[1];
		doLog($t_status);
		doLog($t_task_for);

		$prj = Vtiger_Record_Model::getInstanceById( $proj_id, 'Project' )->getEntity()->column_fields;
		$sale_id = $prj['salesorder_id'];

		$sale = Vtiger_Record_Model::getInstanceById( $sale_id, 'SalesOrder' )->getEntity()->column_fields;
                $sale_status = $sale['sostatus'];
		$salesorder_no = $sale['salesorder_no'];
		$accnt_manager_id = $sale['assigned_user_id'];
		$accnt_id = $sale['account_id'];

                doLog($sale_status);
                doLog($accnt_manager_id);
                doLog($accnt_id);
		
		$accnt = Vtiger_Record_Model::getInstanceById( $accnt_id, 'Accounts' )->getEntity()->column_fields;
                $accnt_name = $accnt['accountname'];
                doLog($accnt_name);

		$host_name = $GLOBALS['MQ_CLIENT_HOST'];
		$proto_col = $GLOBALS['MQ_PROTOCOL'];
		$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$entity_id;
		$usr_id = '';
		$role_id = '';
		$sel_que = '';

		$task_complete_flag = 'No';
		$task_pending_flag = 'No';
                if ($sale_status == 'Approved' || $sale_status == 'Provider Delivered'){
                	//Check for first few tasks are completed
                        $task_complete_flag = 'Yes';
                        $query = "SELECT t.projecttaskid, t.projecttaskstatus, t.projecttask_no, tcf.cf_876, p.salesorder_id, p.projectid, p.linktoaccountscontacts, sale.sostatus, scf.cf_884 FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p, vtiger_salesorder sale, vtiger_salesordercf scf WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND p.salesorder_id = scf.salesorderid AND sale.salesorderid = scf.salesorderid AND tcf.cf_876 in ('Configure Router', 'Voice Provision', 'Update SO Status') AND t.projectid = ".$proj_id." order by tcf.cf_876;";
                        $result = $adb->query($query);

                        foreach ($result as $res){
                                if ($res['projecttaskstatus'] != 'Completed'){
                                        $task_complete_flag = 'No';
                                        break;
                                }
                        }

                	//Check for pending tasks
			$task_pending_flag = 'Yes';
			$pending_query = "SELECT t.projecttaskid, t.projecttaskstatus, t.projecttask_no, tcf.cf_876, p.salesorder_id, p.projectid, p.linktoaccountscontacts, sale.sostatus, scf.cf_884 FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p, vtiger_salesorder sale, vtiger_salesordercf scf WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND p.salesorder_id = scf.salesorderid AND sale.salesorderid = scf.salesorderid AND tcf.cf_876 in ('Perform Installation', 'Network Testing', 'Create Circuit Card') AND t.projectid = ".$proj_id." order by tcf.cf_876;";
                        $pending_result = $adb->query($pending_query);

                        foreach ($pending_result as $pend){
                                if ($pend['projecttaskstatus'] == 'Completed'){
                                        $task_pending_flag = 'No';
                                        break;
                                }
                        }

                }
		doLog($task_pending_flag);

		if(($t_status == 'Open') && ($t_task_for == 'Site Survey') && ($sale_status == 'Initiate Planning')){
			//Send Mail to NDM to start site survey task
			$role_id = 'H7';
			$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
			$mail_subject = 'Site Survey Task for ' . $accnt_name . ' asigned to you';
			$mail_content = 'A site survey task for the ' . $accnt_name . ' has been assigned to you. You may access it <a href="'.$task_url.'">here</a> and please assign to the suitable Engineer.';
		}
		else if(($t_status == 'Completed') && ($t_task_for == 'Site Survey') && ($sale_status == 'Initiate Planning')){
			//Send Mail to NPM to start service delivery task
			$query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Service Delivery' AND t.projectid = ?;";
                	$task_result = $adb->pquery($query,array($proj_id));
			$t_id = $adb->query_result($task_result, 0, "projecttaskid");

			$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
			$role_id = 'H6';
			$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
			$mail_subject = 'Site Survey for ' . $accnt_name . ' is completed';
                        $mail_content = 'Site survey event for ' . $accnt_name . ' has been completed and task has been assigned to you to create a plan for delivery. You may access the task details <a href="'.$task_url.'">here</a>';
			
		}
		else if(($t_status == 'Completed') && ($t_task_for == 'Service Delivery') && ($sale_status == 'Initiate Planning')){
			//Find type of product to decide standard or custom
			$typ_query = "select quo.cf_976,quo.quoteid from vtiger_quotescf quo, vtiger_projecttaskcf tas where tas.projecttaskid = quo.cf_978 and tas.projecttaskid = ?;";
                        $type_res = $adb->pquery($typ_query,array($entity_id));
                        $type_of_prd = $adb->query_result($type_res, 0, "cf_976");
                        $quote_id = $adb->query_result($type_res, 0, "quoteid");
			doLog($type_of_prd);
			if ($type_of_prd == 1){
				//Update set price task status
				$up_query = "UPDATE vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p SET projecttaskstatus = 'Completed', projecttaskprogress='100%' WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Set Price' AND t.projectid = ".$proj_id;
				doLog($up_query);
                                $adb->pquery($up_query);
				//Send Mail to AM to start get order sign task
				$query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Order Sign' AND t.projectid = ?;";
				$task_result = $adb->pquery($query,array($proj_id));
				$t_id = $adb->query_result($task_result, 0, "projecttaskid");

				$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
				$usr_id = $accnt_manager_id;
				$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = " . $usr_id;
				$mail_subject = 'A task has been created for ' . $accnt_name . ' '. $salesorder_no .' to get order sign.';
				$mail_content = 'A task has been created for ' . $accnt_name . ' '. $salesorder_no .' to you to approach the customer to sign. You may access the task details <a href="'.$task_url.'">here</a>';

				//Update to Account manager
				$owner_query = 'update vtiger_crmentity set smownerid = '.$usr_id.' where crmid ='.$t_id;
				$adb->pquery($owner_query);
                		doLog('update projecttask ownerid to account ownerid');
			}
			else{
				//Send Mail to PM to start discuss about set price task
				$query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Set Price' AND t.projectid = ?;";
				$task_result = $adb->pquery($query,array($proj_id));
				$t_id = $adb->query_result($task_result, 0, "projecttaskid");

				$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
				$quote_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&relatedModule=Quotes&view=Detail&record='.$entity_id.'&mode=showRelatedList&tab_label=Quotes';
				$role_id = 'H17';
				$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
				$mail_subject = 'Discuss solution for ' . $accnt_name;
				$mail_content = 'Schedule a meeting to discuss the drafted solution and cost for customer ' . $accnt_name . ' potential services. You may access NP Quote details <a href="'.$quote_url.'">here</a>. Also you can access task details  <a href="'.$task_url.'">here</a>';
			}
                }
		else if(($t_status == 'Completed') && ($t_task_for == 'Set Price') && ($sale_status == 'Initiate Planning')){
                        //Send Mail to AM to start get order sign task
			$query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Order Sign' AND t.projectid = ?;";
                        $task_result = $adb->pquery($query,array($proj_id));
                        $t_id = $adb->query_result($task_result, 0, "projecttaskid");

			$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
			$usr_id = $accnt_manager_id;
			$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = " . $usr_id;
			$mail_subject = 'Price has been set for ' . $accnt_name . ' '. $salesorder_no;
                        $mail_content = 'PM has finished raising price options to ' . $accnt_name . ' solution '. $salesorder_no .' and a task has been created for you to approach the customer to sign. You may access the task details <a href="'.$task_url.'">here</a>';
			
			//Update to Account manager
                        $owner_query = 'update vtiger_crmentity set smownerid = '.$usr_id.' where crmid ='.$t_id;
                        $adb->pquery($owner_query);
                        doLog('update projecttask ownerid to account ownerid');
                }
		else if(($t_status == 'Completed') && ($t_task_for == 'Perform Installation') && ($sale_status == 'In Progress')){
			//Send Mail to NMM to issue circuit card
                        $query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND tcf.cf_876 = 'Create Circuit Card' AND t.projectid = ?;";
                        $task_result = $adb->pquery($query,array($proj_id));
                        $t_id = $adb->query_result($task_result, 0, "projecttaskid");

			$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
                        $role_id = 'H8';
                        $sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
                        $mail_subject = $t_no.' for '.$accnt_name.' assigned to you';
                        $mail_content = 'Task to issue a Circuit Card to' . $accnt_name . ' for '. $salesorder_no .' has been assigned to you. You may assign this to a suitable engineer. You will find more details on the task <a href="'.$task_url.'">here</a>';
			}
		else if(($t_status == 'Completed') && ($t_task_for == 'Configure Router') && ($sale_status == 'Approved')){

                        //Send Mail to NDM to Physical Installation
			$t_id_1 = $adb->query_result($pending_result, 1, "projecttaskid");	
                        $task_url_1 = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id_1;
                        $role_id_1 = 'H7';
                        $sel_que_1 = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id_1 . "'";
                        $mail_subject_1 = $t_no.' for '.$accnt_name.' assigned to you';
                        $mail_content_1 = 'Task for physical installation to ' . $accnt_name . ' for '. $salesorder_no .' has been assigned to you. You may assign this to a suitable engineer. You will find more details on the task <a href="'.$task_url_1.'">here</a>';

			//Update sales order status to In Progress
                        $sale_query = 'update vtiger_salesorder set sostatus = "In Progress" where salesorderid = '.$sale_id;
                        $adb->pquery($sale_query);
                        doLog('update salesorder status to In Progress');

                }
		else if(($t_status == 'Completed') && ($task_complete_flag == 'Yes') && ($task_pending_flag == 'Yes') && ($sale_status == 'Provider Delivered')){
                        //Send Mail to NMM to Network Testing
			$t_id = $adb->query_result($pending_result, 0, "projecttaskid");	
                        $task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id;
                        $role_id = 'H8';
                        $sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
                        $mail_subject = $t_no.' for '.$accnt_name.' assigned to you';
                        $mail_content = 'Task to test the Provider Circuit for ' . $accnt_name . ' for '. $salesorder_no .' has been assigned to you. You may assign this to a suitable NM engineer. You will find more details on the task <a href="'.$task_url.'">here</a>';
                        
			//Send Mail to NDM to perform physical works
			$t_id_1 = $adb->query_result($pending_result, 1, "projecttaskid");	
                        $task_url_1 = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$t_id_1;
                        $role_id_1 = 'H7';
                        $sel_que_1 = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id_1 . "'";
                        $mail_subject_1 = $t_no.' for '.$accnt_name.' assigned to you';
                        $mail_content_1 = 'Task to perform physical works for ' . $accnt_name . ' for '. $salesorder_no .' has been assigned to you. You may assign this to a suitable ND engineer. You will find more details on the task <a href="'.$task_url_1.'">here</a>';
                }


		$task_array = array("Configure Router");
		$sos_array = array("Approved", "Provider Delivered");

		if(($t_status == 'Completed') && (in_array($t_task_for, $task_array)) && (in_array($sale_status, $sos_array)) && ($circ_id!='')){
			$pot_query = 'update vtiger_potential pot, vtiger_potentialscf pcf, vtiger_salesorder sale set pcf.cf_954 = "'.$circ_id.'", pcf.cf_956="'.$sub_id.'" where pot.potentialid = pcf.potentialid and pot.potentialid = sale.potentialid and sale.salesorderid = '.$sale_id;
	                $adb->pquery($pot_query);
			doLog($pot_query);
        	        doLog('Updated circuitID and subscriberID in Potential');
		}
		
		if($sel_que != ''){		
			doLog($sel_que);

			//Update task status to open
                        $status_query = 'update vtiger_projecttask set projecttaskstatus = "Open" where projecttaskid ='.$t_id;
                        $adb->pquery($status_query);
                        doLog('update projecttask status to open');

        		$result = $adb->query($sel_que);
                	$fields = $result->fields;
			$row_count = $result->_numOfRows;
			doLog($row_count);
			if ($row_count > 1){
				foreach ($result as $field){
					$mail_text = 'Hi '.$field['first_name'].' '.$field['last_name'].',<br /><br />'.$mail_content;
					$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $field['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text,'','','');
				}
			}
			else{
				$mail_text = 'Hi '.$fields['first_name'].' '.$fields['last_name'].',<br /><br />'.$mail_content;
				$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $fields['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text,'','','');
			}
		}

		if($sel_que_1 != ''){

			//Update task status to open
                        $status_query = 'update vtiger_projecttask set projecttaskstatus = "Open" where projecttaskid ='.$t_id_1;
                        $adb->pquery($status_query);
                        doLog('update projecttask status to open');
			
                        doLog($sel_que_1);
                        $result_1 = $adb->query($sel_que_1);
                        $fields_1 = $result_1->fields;
                        $row_count_1 = $result_1->_numOfRows;
                        if ($row_count_1 > 1){
                                foreach ($result_1 as $field_1){
					$mail_text_1 = 'Hi '.$field_1['first_name'].' '.$field_1['last_name'].',<br /><br />'.$mail_content_1;
                                        $status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $field_1['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject_1, $mail_text_1,'','','');
                                }
                        }
                        else{
				$mail_text_1 = 'Hi '.$fields_1['first_name'].' '.$fields_1['last_name'].',<br /><br />'.$mail_content_1;
                                $status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $fields_1['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject_1, $mail_text_1,'','','');
                        }
                }
	
	}
 
	//Send Mail Function
	send_notification($entity, $adb);
	doLog("---------- End Process - Send_Mail_Notification ----------");
	
}

?>
