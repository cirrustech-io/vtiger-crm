<?php
/*
 * @title		Workflow Function - Auto Populate
 * @description		Workflow function triggered for Auto populating fields
 */

require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once('vendor/autoload.php');

function update_sitesurvey_field( $entity ) 
{
      
	doLog("-------------------------Entering Custom Workflow Function: Auto Update Sitesurvey field-----------------------");

	global $log, $adb;
        $entityData = $entity->data;
	$id = explode("x", $entityData['id']);
        $entity_id = $id[1];
        doLog("Trigerring Entity Id : ".$entity_id); 

	$sel_que = 'select smownerid from vtiger_crmentity where crmid ='.$entity_id;
        doLog($sel_que);
	$owner_data = $adb->query($sel_que);
        $owner = $adb->query_result($owner_data,0,"smownerid");

	$up_que = 'update vtiger_projecttask task, vtiger_sitesurveyscf scf, vtiger_crmentity crm set crm.smownerid = '.$owner.' where task.projecttaskid = scf.cf_866 and crm.crmid = scf.sitesurveysid and scf.cf_866='.$entity_id;
	doLog($up_que);
	$adb->pquery($up_que);
	doLog("-------------------------Exiting Custom Workflow Function: Auto Update SiteSutvey field--------------------");
	
}

?>
