<?php
/*
 * Workflow Function 
 */


// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Send_Potential( $entity ) 
{
	
	doLog("---------- Start Process - MQ_Send_Potentials ----------");
        doLog($GLOBALS['MQ_EXCHANGE_NAME']);

        $routingKey = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_account']['routing_key'];
        $queueName = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_account']['queue_name'];
		
	function getSalesOrders( $potential_id ) {
		/*
		Get list of active sales orders for the potential
		*/
		$moduleName = 'Potentials';
		$relatedModuleName = 'SalesOrder';
		$pagingModel = new Vtiger_Paging_Model();
	
		$parentRecordModel = Vtiger_Record_Model::getInstanceById($potential_id, $moduleName);
		$relationListView = Vtiger_RelationListView_Model::getInstance($parentRecordModel, $relatedModuleName );
		$models = $relationListView->getEntries($pagingModel);
		$noOfEntries = count($models);
		
		$salesOrders = Array();
		foreach ( $models as $key => $value ) {
			$thisSalesOrder = Vtiger_Record_Model::getInstanceById($key, $relatedModuleName)->getEntity()->column_fields;
			if( $thisSalesOrder['sostatus'] == 'Approved' ) {
				$salesOrders[] = $thisSalesOrder;
			}
		}
	
		$soCount = sizeof($salesOrders);

		// Only one active sales order should be present
		if( $soCount > 1 || $soCount == 0  ) {
			return false;
		}
		else {
			return $salesOrders[0];
		}
		
	} // end function getSalesOrders
	
	global $vtiger_current_version;
		
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_Potential_ClosedWon.php',
		'description' => 'Workflow function triggered on Opportunity save which submits the opportunity and related data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
        $json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
        $json->_appinfo->mq = new stdClass();
        $json->_appinfo->mq = Array(
                'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
                'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
                'routing_key' => $routingKey,
                'queue' => $queueName,
        );
	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();
	$json->payload->account = Array();
	$json->payload->contact = Array();
	$json->payload->product = Array();
	$json->payload->salesorder = Array();

		
	// Potential Information
	$moduleName = $entity->moduleName;
	$potential_long_id = explode( 'x', $entity->id );
	$potId = $potential_long_id[1];
	$potData = $entity->data;

	// Get Account
	$accountModuleName = 'Accounts';
	$acctId = explode('x', $potData['related_to']);
	$account = Vtiger_Record_Model::getInstanceById( $acctId[1], 'Accounts' )->getEntity()->column_fields;
	$json->payload->account = $account;
	
	// Get Sales Order
	$json->payload->salesorder = getSalesOrders( $potId );
	
	if( $json->payload->salesorder ){
		// Get Product associated with Sales Order
		$product_id = $json->payload->salesorder['productid'];
		$json->payload->product = Vtiger_Record_Model::getInstanceById( $product_id, 'Products' )->getEntity()->column_fields;
		// Get Contact associated with Sales Order
		$contact_id = $json->payload->salesorder['contact_id'];
		$json->payload->contact = Vtiger_Record_Model::getInstanceById( $contact_id, 'Contacts' )->getEntity()->column_fields;
	}
	
	if( $json->payload->salesorder && submitQueue( json_encode( $json ), $routingKey ) ) {
		$comment_message = 'MQ_Potential_ClosedWon.php Workflow Function was Triggered which sent a message in to the queue successfully.';
	} else {
		$comment_message = 'ERROR! SalesOrders=0 OR SalesOrders>1';
	}
	
	doLog( json_encode( $json ) );
	doLog("---------- End Process - MQ_Send_Potential ----------");	
	
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	$comment->column_fields['related_to'] = $potId;
	#$comment->column_fields['userid'] = 5;
	$comment->save('ModComments');
	
	return true;

}

?>

