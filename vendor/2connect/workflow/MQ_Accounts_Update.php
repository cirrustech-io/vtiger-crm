<?php
/*
 * @title			Workflow Function - MQ_Send_Accounts
 * @description		Workflow function triggered on Accounts save which submits the account and related data to MQ.
 */

// composer autoloader required for AMQP libraries

require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Send_Accounts( $entity ) 
{

	doLog("---------- Start Process - MQ_Accounts_Update ----------");
        doLog($GLOBALS['MQ_EXCHANGE_NAME']);

        $routingKey = $GLOBALS['MQ_QUEUE_MAP']['workflow_update_account']['routing_key'];
        $queueName = $GLOBALS['MQ_QUEUE_MAP']['workflow_update_account']['queue_name'];
		
	// Load Vtiger version number
	global $vtiger_current_version;
	
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	// $json->_appinfo->application = 'Vtiger CRM 6.1.0';
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_Accounts_Update.php',
		'description' => 'Workflow function triggered on Accounts save which submits the account and related data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
        $json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
        $json->_appinfo->mq = new stdClass();
        $json->_appinfo->mq = Array(
                'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
                'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
                'routing_key' => $routingKey,
                'queue' => $queueName,
        );


	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();
	$json->payload->account = $entity->data;
	
	// Submit message to queue and generate comment for related Opportunity
	if( submitQueue( json_encode( $json ), $routingKey ) ) {
		$comment_message = "Accounts " . $json->payload->account['account_no'] . " created and submitted to MQ for QuickBooks synchronization.";
	} else {
		$comment_message = "Problem with update Accounts " . $json->payload->account['account_no'] . " submission to MQ for QuickBooks synchronization.";
	}
        
		
	// Create a comment for related Opportunity
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	#$comment->column_fields['related_to'] = $potential_id;
	// $comment->column_fields['userid'] = 5;
	
	// Development only
	doLog( json_encode( $json ) );
	doLog("---------- End Process - MQ_Update_Account ----------");
	
	if( $comment->save('ModComments') ) {
		return true;
	} else {
		return false;
	}
	
}

?>
