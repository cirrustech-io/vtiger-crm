<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

function Batelco_Batch_AutoCreateFn($entity) {
 
        global $current_user, $adb, $log;

	$entity_data = $entity->data;

        function doDebug( $data ) {
                /*
                 * Write $data to text log file on filesystem
                 */
                $file = 'logs/workflow_create_batelco_batch.log';
                file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
                file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );
        } // end function doDebug

	// Just get the current date
        $startdate = date("Y/m/d");
        $targetdate = date("Y/m/d",strtotime("+44 days"));	

	$htmlFile = '<html>
		     <head>
		     </head>
		    <body>
		    <div>
			Mr. Ali AbdulHameed <br />
			SMU <br />
			Batelco <br />
			P.O. Box 14 <br />
			Kingdom Of Bahrain <br />
			<br /> <br />
			' .$startdate. '
			<br /> <br />
			Re: BTS Transfer Request Batch No. B-BTS-' .$startdate. '-01
			<br /> <br />
			Dear Ali/Noaf,
			<br /> <br />
			Please find enclosed Order Consulting Batch No. <br /> 
			B-BTS-' .$startdate. '-01 for the following Circuit Numbers / Account name:
			<br /> <br />
			
		    </div>
		    <div>
		    <table style="border-collapse: collapse; width: 100%;">
		     <tbody>
		      <tr>
		<th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">No</th>
		<th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Customer Name</th>
		<th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Circuit Number</th>
		      </tr>
		      <tr>
		       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">1</td>
		       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">Style Hotel Management</td>
		       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">17365274</td>
		      </tr>
		      <tr>
		       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">1</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">Style Hotel Management</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">17365274</td>
                      </tr>
		     </tbody>
		    </table>
		</div>
		<br />

		<div>
		    Please do not hesitate to contact me if you require any clarifications.
		    <br /><br /><br /><br />
		    Prepared and Checked by <br/>
		    <br /><br />
		    Charles Christopher
		    <br /><br />
		</div>
		<br /> <br />
		</body>
		</html>';

        doDebug("Entering Custom Workflow Function: Create Batelco Batch");
        doDebug($htmlFile);

	function html_to_pdf($htmlFile){
	    
		require_once("html2pdf/html2fpdf.php");
		$myPDF = new HTML2FPDF();
		$myPDF->HTML2FPDF("P","mm","A4");
		$myPDF->AddPage();
		$myPDF->WriteHTML($htmlFile);
		$myPDF->Output('test.pdf', 'F'); 
	}

	html_to_pdf($htmlFile);
        doDebug("Exit Custom Workflow Function...");
}
?>
