<?php
/*
 * @title		Workflow Function - Auto Populate
 * @description		Workflow function triggered for Auto populating fields
 */

require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once('vendor/autoload.php');

function update_product_field( $entity ) 
{
      
	doLog("-------------------------Entering Custom Workflow Function: Auto Update Product field-----------------------");

	global $log, $adb;
        $entityData = $entity->data;
	$id = explode("x", $entityData['id']);
        $entity_id = $id[1];
        doLog("Trigerring Entity Id : ".$entity_id);
	$prd_code = $entityData['productcode'];
	$prd_name = $entityData['productname'];
	
	$speed = explode('(', $prd_name);
	if (count($speed) > 1){
        	$prd_speed = str_replace(")","",$speed[1]);
	    	$up_query = "update vtiger_products prd, vtiger_productcf pcf set pcf.cf_990 = '".$prd_speed."' where prd.productid = pcf.productid and prd.productcode = '".$prd_code."';";
		doLog($up_query);
		$adb->pquery($up_query);
	}
	doLog("-------------------------Exiting Custom Workflow Function: Auto Update Product field--------------------");
	
}

?>
