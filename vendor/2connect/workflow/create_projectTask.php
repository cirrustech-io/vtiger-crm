<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );

function ProjectTask_WorkflowTask_AutoCreateFn($entity) {
 
        global $current_user, $adb, $log;

	$entity_data = $entity->data;

        doLog( $entity_data );
 
        doLog("Entering Custom Workflow Function: Create ProjectTask");

	
        include_once 'include/utils/CommonUtils.php';
        include_once 'include/database/PearDatabase.php';
 
        // Get the user of the last modifier (or owner) of the entity that triggered this workflow
        $id = explode("x", $entity_data['id']);
        $potid = $id[1];       
       
        doLog("Querying for user id (".$potid.")...");
        $query = "SELECT smcreatorid, modifiedby FROM vtiger_crmentity WHERE crmid = ?";
        $result = $adb->pquery($query,array($potid));
       
        $modby = $adb->query_result($result,0,"modifiedby");   
        $creby = $adb->query_result($result,0,"smcreatorid");
       
        // If you want the "username" use function getUserName($id)
        if ($modby == 0 ) {
                // Entity not been modified so use creator's ID
                $uname = getUserFullName($creby);
        } else {
                $uname = getUserFullName($modby);
        }
 
        // Build the information for the new ProjectTask
	$salesorder_id = $entity_data['salesorder_id'];
        $project_name = $entity_data['projectname'];
        $related_to = $entity_data['projectid'];
        $assigned_to = $entity_data['assigned_user_id'];
	doLog($project_name);
 
        // Just get the current date
	$startdate = date("Y/m/d");
	$targetdate = date("Y/m/d",strtotime("+30 days"));


	$taskdetails = array ( "task"  =>  array ( "taskfor" => "Site Survey", "name" => "ND Task to create site survey"),
				     	   array ( "taskfor" => "Service Delivery", "name" => "NP Task for service delivery"),
					   array ( "taskfor" => "Set Price", "name" => "AM Task to set price"),
					   array ( "taskfor" => "Order Sign", "name" => "AM Task to get order signed"),
			);

	$task = $taskdetails['task'];
	foreach ($task as $value{
		$task_for = value['taskfor'];
		$name = value['name'];
        	$description = "This ProjectTask " .$name.  "was automatically created when a Project is created when SalesOrder "
                                 . $entity_data['salesorder_id'] . " status was changed to 'Initiate Planning' by " . $uname . ".";

       		// Assemble the necessary ProjectTask fields        
        	$parameters = array(

		    'projecttaskname' => $name,
		    'projecttasktype' => 'operative'
		    'projecttaskpriority' => 'high', 
		    'assigned_user_id' => $assigned_to,
		    'projecttaskprogress' => '10%'
		    'startdate' => $startdate,
		    'enddate' => $targetdate,
		    'description' => $description,
		    'projecttaskstatus' => 'Open',
		    'cf_876' => $task_for,
		    'projectid' => $related_to,

		);
	 
		// Create the new ProjectTask
		include_once 'include/Webservices/Create.php';
		doLog($parameters);
		//vtws_create('ProjectTask', $parameters, $current_user);
		doLog("Finished entering " .$name. " and exiting.");
	}
	doLog("Exit Custom Workflow Function...");
	
}
?>
