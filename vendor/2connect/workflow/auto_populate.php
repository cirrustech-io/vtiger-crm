<?php
/*
 * @title		Workflow Function - Auto Populate
 * @description		Workflow function triggered for Auto populating fields
 */

require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once('vendor/autoload.php');

function populate_field( $entity ) 
{
      
	doLog("-------------------------Entering Custom Workflow Function: Auto Populate field-----------------------");

	global $log, $adb;
        $entityData = $entity->data;
	$name = '';
	$que = '';
	$id = explode("x", $entityData['id']);
        $entity_id = $id[1];
        doLog("Trigerring Entity Id : ".$entity_id); 

	if(strpos($entityData['id'], '13x') === true){
 		$name = $entityData['potential_no'];
		$que = 'update vtiger_potential set potentialname="'.$name.'" where potentialid='.$entity_id;
	}
	else{
		$lineitems = $entityData['LineItems'];
		foreach ($lineitems as $line){
			$product_id = explode('x', $line['productid']);
                	$prod_id = $product_id[1];
			$prd = Vtiger_Record_Model::getInstanceById($prod_id, 'Products')->getEntity()->column_fields;
			if($prd['cf_960']  == 'Yes'){
				$name = $prd['productname'];
				$prd_cat = $prd['productcategory'];
				break;
			}
		}
		$que = 'update vtiger_potential pot, vtiger_salesorder sale, vtiger_salesordercf scf set pot.potentialname = "'.$name.'", sale.subject = "'.$name.'", scf.cf_974 = "'.$prd_cat.'" where pot.potentialid = sale.potentialid and sale.salesorderid = scf.salesorderid and sale.salesorderid = '.$entity_id;
	}

	doLog($name);
	doLog($prd_cat);

	if($name!=''){
		doLog($que);
		$adb->pquery($que);
		doLog("-------------------------Exiting Custom Workflow Function: Auto Populate field--------------------");
	}
	
}

?>
