 <?php
    /*+**********************************************************************************
     * The contents of this file are subject to the vtiger CRM Public License Version 1.0
     * ("License"); You may not use this file except in compliance with the License
     * The Original Code is:  vtiger CRM Open Source
     * The Initial Developer of the Original Code is vtiger.
     * Portions created by vtiger are Copyright (C) vtiger.
     * All Rights Reserved.
     ************************************************************************************/
     
    /**
     * This method is registered with workflow as custom function to be invoked via Task.
     * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
     */

    require_once('config.mq.php');
    require_once('include/utils/utils.php');
    require_once('modules/Emails/mail.php');
    require_once('vendor/2connect/workflow/MQ_Helper.php');
    require_once( 'vendor/autoload.php' );

    function EventFromSalesOrder_Workflow_AutoCreateFn($entity) {
     
            global $current_user, $adb, $log;

	    $entityData = $entity->data;
            $entity_id = str_replace("6x","",$entityData['id']);
     
            doLog("---------Entering Custom Workflow Function: Create Entity from (".$entityData['subject'].")... -------");
            doLog("Trigerring SalesOrder is: ".$entity_id.".");
     
            include_once 'include/utils/CommonUtils.php';
            include_once 'include/database/PearDatabase.php';
     
            // Get the saleid
            $id = explode("x", $entityData['id']);
            $saleid = $id[1];
	    $status = $entityData['sostatus'];
	    $process_type = $entityData['cf_884'];
	    $assigned_to = '20x56';
            $date = date("Y/m/d");
	    $name = '';
	    $mail_content = '';

            // Build the information for the new Event
	    if ($status == 'Approved' && $process_type == 'Bitstream'){
	    	$assigned_to = '20x19';
	    }
	    if($status == 'Approved' && $process_type == 'Offnet'){
		$name = 'Submit Offnet order to provider';
                $description = 'This Event was automatically created for operation officer to take necessary action to send the order to provider.';
	    }

	    doLog($process_type);
	    doLog($status);
	    doLog($assigned_to);

	    //Assign Sales Order to responsible
            $sm_owner = explode("x", $assigned_to);
            $sm_owner_id = $sm_owner[1];
            $que = "UPDATE vtiger_crmentity SET smownerid = '". $sm_owner_id ."' WHERE crmid=" . $saleid;
            doLog($que);
            $adb->pquery($que);

	    if($name != ''){
            	// Assemble the necessary Event fields        
            	$parameters = array(
                    'subject' => $name,
		    'activitytype' => 'Task',
                    'date_start' => $date,
                    'due_date' => $date,
                    'time_start' => '10:00:00',
                    'time_end' => '12:00:00',
		    'eventstatus' => 'Not Started',
		    'visibility' => 'Private',
                    'description' => $description,
                    'duration_hours' => 0,
                    'assigned_user_id' => $assigned_to,
                    'parent_id' => $entityData['id']
            	);
		doLog($parameters);

            	// Create the new Event
            	include_once 'include/Webservices/Create.php';
            	doLog("Creating new Event from SalesOrder for : ".$parameters."...");
            	$resp = vtws_create('Events', $parameters, $current_user);
            	doLog($resp);

		//Send Mails to responsible person
		$event = explode("x", $resp["id"]);
                $event_id = $event[1];
		$host_name = $GLOBALS['MQ_CLIENT_HOST'];
		$event_url = 'http://'.$host_name.'/index.php?module=Calendar&view=Detail&record='.$event_id;
		$sale_url = 'http://'.$host_name.'/index.php?module=SalesOrder&view=Detail&record='.$saleid;
		$mail_content = 'This Event is created to you. Please click <a href="'.$event_url.'">here</a> to follow the event and do the needfull. Click <a href="'.$sale_url.'">here</a> to access sales order.';

		$sel_que = "select usr.email1, usr.user_name, usr.first_name, usr.last_name, grp2role.roleid, grp.groupid from vtiger_users usr, vtiger_user2role usr2role, vtiger_group2role grp2role, vtiger_groups grp where grp.groupid = grp2role.groupid and grp2role.roleid = usr2role.roleid and usr2role.userid = usr.id and grp.groupid =" .$sm_owner_id;
		doLog($sel_que);
		$results = $adb->query($sel_que);
	
		foreach ($results as $result) {
			$mail_text = 'Hi '.$result['first_name'].' '.$result['last_name'].',<br /><br />'.$mail_content;
			$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $result['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $name, $mail_text,'','','');	
		}
            	doLog("--------------------Exit Custom Workflow Function... - create_event_from_salesorder------------------");
	}
    }
    ?>


