<?php
/*
 * @title               Workflow Function - Helper file for MQ
 * @description         It's a MQHelper file
 */
// composer autoloader required for AMQP libraries

require_once('vendor/autoload.php');
require_once('include/utils/utils.php');
require_once('config.mq.php');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function doLog($data) {
	/*
	 * Write $data to text log file on filesystem
	 */
	$file = $GLOBALS['MQ_LOG_FILE_PATH'];
	file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
	file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );
} // end function doLog

function submitQueue($json_data, $routingKey) {
	/*
	 * Received data should be in JSON. This data is submitted to MQ 
	 * TODO: take MQ information (vhost, exchange, routingKey) as parameters to be reusable
	 * TODO: verify that received data is JSON
	 */
	$host = $GLOBALS['MQ_HOST_NAME'];
	$port = $GLOBALS['MQ_PORT'];
	$username = $GLOBALS['MQ_USER_NAME'];
	$password = $GLOBALS['MQ_PASSWORD'];
	$vhost = $GLOBALS['MQ_VIRTUAL_HOST'];
	$exchange = $GLOBALS['MQ_EXCHANGE_NAME'];
	#$routingKey = 'workflow.new_account';

	$connection = new AMQPConnection($host, $port, $username, $password, $vhost);
	$channel = $connection->channel();

	#$channel->queue_declare('hello', false, false, false, false);

	$msg = new AMQPMessage($json_data, array('content_type' => $GLOBALS['MQ_CONTENT_TYPE'], 'delivery_mode' => $GLOBALS['MQ_DELIVERY_MODE']));
	$channel->basic_publish($msg, $exchange, $routingKey);

	$channel->close();
	$connection->close();

	return true;
} // end function submitQueue

?>

