<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

$servername = "localhost";
$username = "root";
$password = "3x1651a";
$dbname = "vtiger_production";
$homepath = '/var/www/vtigercrm/';

require_once($homepath.'include/utils/utils.php');
require($homepath."modules/Emails/mail.php");


function Batelco_Batch_Scheduler() {
 
        function doDebug( $data ) {
                /*
                 * Write $data to text log file on filesystem
                 */
                $file = $GLOBALS['homepath'].'logs/batelco_batch_scheduler.log';
                file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
                file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );
        } // end function doDebug

	// Prepare data from vtiger sales order
	function get_data_from_db(){

		// Create connection
		$conn = new mysqli($GLOBALS['servername'], $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname']);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		// Just get the current date
                $mod_sdate = date("Y-m-d");
                $mod_edate = date("Y-m-d",strtotime("+1 days"));
		doDebug($mod_sdate);
		doDebug($mod_edate);


		$sql = "select s.salesorderid, s.salesorder_no, scf.cf_815, scf.cf_890, acc.accountid, acc.accountname, acccf.cf_827, crm.modifiedtime from vtiger_salesorder s, vtiger_salesordercf scf, vtiger_account acc, vtiger_accountscf acccf, vtiger_crmentity crm where s.salesorderid = scf.salesorderid and s.accountid = acc.accountid and s.salesorderid = crm.crmid and acc.accountid = acccf.accountid and s.sostatus = 'Approved' and scf.cf_815 != '' and scf.cf_890 !='' and crm.modifiedtime >= '".$mod_sdate."' and crm.modifiedtime < '".$mod_edate."';";
		 doDebug($sql);
		$result = $conn->query($sql);
		$r_result = $conn->query($sql);
		$row_cnt = $result->num_rows;

		// Just get the current date
        	$startdate = date("d.m.Y");
        	$targetdate = date("d.m.Y",strtotime("+44 days"));

        	$htmlFile = '<html>
                     <head>
                     </head>
                    <body>
		    <br /> <br />
                    <br /> <br />
                    <br /> <br />
                    <div>
			<br />
                        Mr. Ali AbdulHameed <br />
                        SMU <br />
                        Batelco <br />
                        P.O. Box 14 <br />
                        Kingdom Of Bahrain <br />
                        <br /> <br />
                        ' .$startdate. '
                        <br /> <br />
                        Re: BTS Transfer Request Batch No. B-BTS-' .$startdate. '-' .$row_cnt. '
                        <br /> <br />
                        Dear Ali/Noaf,
                        <br /> <br />
                        Please find enclosed Order Consulting Batch No. <br /> 
                        B-BTS-' .$startdate. '-01 for the following Circuit Numbers / Account name:
                        <br /> <br />
                    </div>
                    <div>';
		
		if ($result->num_rows > 0) {
		    $htmlFile .= '<table style="border-collapse: collapse; width: 100%;">
                     <tbody>
                      <tr>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">No</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Customer Name</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Circuit Number</th>
                      </tr>';
		    // output data of each row
		    $count = 0;
		    while($row = $result->fetch_assoc()) {
			$count += 1;
			$htmlFile .= '<tr>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$count.'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["accountname"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_890"].'</td>
                      </tr>';
		    }
		    $htmlFile .= '</tbody>
                    </table>';
		 } else {
                    $htmlFile .='0 results';
                }

		$htmlFile .='</div>
                <br />
                <div>
		    <br />
                    Please do not hesitate to contact me if you require any clarifications.
                    <br /><br />
                    <br /><br />
                    Prepared and Checked by <br/>
                    <br /><br />
                    <br /><br />
                    Charles Christopher
                    <br /><br />
                    <br /><br />
                </div>
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />
                <br /> <br />';
	
		if ($result->num_rows > 0) {
                    $htmlFile .= '<table style="border-collapse: collapse; width: 100%;">
                     <tbody>
                      <tr>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">No</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Customer Name</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Circuit Number</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">User Id</th>
                <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">CPR/CR</th>
              <th style="background-color:#f1f1f1; border: 1px solid #d4d4d4; padding: 7px 5px; width:20%;  vertical-align: top;">Batelco Comments</th>
                      </tr>';
                    // output data of each row
                    $r_count = 0;
                    while($row = $r_result->fetch_assoc()) {
                        $r_count += 1;
                        $htmlFile .= '<tr>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$r_count.'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["accountname"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_890"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_815"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;">'.$row["cf_827"].'</td>
                       <td style="border: 1px solid #d4d4d4; padding: 7px 5px; vertical-align: top; width:30%;"></td>
                      </tr>';
                    }
                    $htmlFile .= '</tbody>
                    </table>';
                 } else {
                    $htmlFile .='0 results';
                }

                $htmlFile .= '</body>
                </html>';

		$conn->close();
		return array($htmlFile,$row_cnt,$startdate);
	}

	$data = get_data_from_db();	
	
        doDebug("Batelco Batch Creation");
        dioDebug($data);

	function send_mail_to_batelco(){

		$status=send_mail("Batelco Batch File",'binny.abraham@2connectbahrain.com','2Connect Support','crm@i.2connectbahrain.com','Batelco Batch File','Kindly go through attached file','','crm@i.2connectbahrain.com');

	}
	send_mail_to_batelco();

	function html_to_pdf($data){
		
		$htmlFile = $data[0];
        	$row_count = $data[1];
        	$date = $data[2];
	   	
		$file_path =  $GLOBALS['homepath'].'html2pdf/html2fpdf.php';
		$batch_file_name = 'B-BTS-' .$date. '-' .$row_count.'.pdf';
		$output_file = $GLOBALS['homepath'].'batchfiles/'.$batch_file_name;
		require_once($file_path);
		$myPDF = new HTML2FPDF();
		$myPDF->HTML2FPDF("P","mm","A4");
		$myPDF->AddPage();
		$myPDF->WriteHTML($htmlFile);
		$myPDF->Output($output_file, 'F'); 
        	doDebug("Batelco Batch Created Sucessfully");
	}

	html_to_pdf($data);
        doDebug("Exit Batelco Batch Sceduler Function...");
}

Batelco_Batch_Scheduler();
?>
