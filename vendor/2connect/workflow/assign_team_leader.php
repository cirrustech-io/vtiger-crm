<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );

function update_lead( $entity ) 
{
       
	global $log, $adb;
        $entityData = $entity->data;
        $entity_id = str_replace("10x","",$entityData['id']);
	doLog("-----------------------Entering into function update_lead(".$entity_id.")------------------");

	function assignToTeamLeader($entityData, $adb){
		/*
		 check the user role is Sales Manager (H4) or Sales Person (H5)
		*/
		$ent_id = str_replace("10x","",$entityData['id']);
	        $lead_comp = $entityData['company'];
        	$host_name = $GLOBALS['MQ_CLIENT_HOST'];
        	$lead_url = 'http://'.$host_name.'/index.php?module=Leads&view=Detail&record='.$ent_id;
        	doLog($lead_comp);
        	doLog($lead_url);

		$sel_que = "SELECT crm.crmid, u2r.userid, u2r.roleid FROM vtiger_crmentity crm, vtiger_user2role u2r WHERE crm.smcreatorid = u2r.userid and crm.crmid = " . $ent_id;

        	$result = $adb->query($sel_que);
                $fields = $result->fields;
                $roleid = $fields['roleid'];
                $user_id = $fields['userid'];
		#$roleid = array_value($fields), 'roleid');
                doLog($roleid);
                if($roleid!='H5'){
			$roleid = 'H4';
			$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $roleid . "'";
			#$userData = $adb->query("SELECT * FROM vtiger_user2role WHERE roleid = '" . $roleid ."';");
			$userData = $adb->query($sel_que);
			$user_fields = $userData->fields;
			$user_id = $user_fields['id'];
			$mail_subject = 'Lead for '.$lead_comp.' has been created.';
			$mail_content = 'Lead for '.$lead_comp.' has been created. Please assign it to suitable Account Manager. You may access the Lead details <a href="'.$lead_url.'">here</a>';
			$mail_text = 'Hi '.$user_fields['first_name'].' '.$user_fields['last_name'].',<br /><br />'.$mail_content;

			$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $user_fields['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text,'','','');
		}

		$que = "UPDATE vtiger_crmentity SET smownerid = ". $user_id ." WHERE crmid=" . $ent_id;
	        doLog($que);
        	$adb->pquery($que);

	}
 
	//Assign to Account Manager
	assignToTeamLeader($entityData, $adb);
	doLog("------------------------Exit from function update_lead(".$entity_id.")--------------------");
	
}

?>
