 <?php
    /*+**********************************************************************************
     * The contents of this file are subject to the vtiger CRM Public License Version 1.0
     * ("License"); You may not use this file except in compliance with the License
     * The Original Code is:  vtiger CRM Open Source
     * The Initial Developer of the Original Code is vtiger.
     * Portions created by vtiger are Copyright (C) vtiger.
     * All Rights Reserved.
     ************************************************************************************/
     
    /**
     * This method is registered with workflow as custom function to be invoked via Task.
     * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
     */

    require_once('config.mq.php');
    require_once('include/utils/utils.php');
    require_once('modules/Emails/mail.php');
    require_once('vendor/2connect/workflow/MQ_Helper.php');
    require_once( 'vendor/autoload.php' );

    function CancelledEventFromSalesOrder_Workflow_AutoCreateFn($entity) {
     
            global $current_user, $adb, $log;

	    $entityData = $entity->data;
            $entity_id = str_replace("6x","",$entityData['id']);
     
            doLog("---------Entering Custom Workflow Function: Create Entity from (".$entityData['subject'].")... -------");
            doLog("Trigerring SalesOrder is: ".$entity_id.".");
     
            include_once 'include/utils/CommonUtils.php';
            include_once 'include/database/PearDatabase.php';
     
            // Get the saleid
            $id = explode("x", $entityData['id']);
            $saleid = $id[1];
	    $status = $entityData['sostatus'];
	    $process_type = $entityData['cf_884'];
	    $sale_no = $entityData['salesorder_no'];

            $date = date("Y/m/d");
            $date = date("Y/m/d",strtotime("+1 days"));
	    $name = '';
	    $mail_subject = '';
	    $mail_content = '';
	    $mail_text = '';
	    $host_name = $GLOBALS['MQ_CLIENT_HOST'];

	    $acc = explode("x", $entityData['account_id']);
            $acc_id = $acc[1];

	    $acc_data = Vtiger_Record_Model::getInstanceById( $acc_id, 'Accounts' )->getEntity()->column_fields;
	    $user_id = $acc_data['assigned_user_id'];

	    $account_name = $acc_data['accountname'];
	    $sale_url = 'http://'.$host_name.'/index.php?module=SalesOrder&view=Detail&record='.$saleid;

            // Build the information for the new Event
	    if ($status == 'Cancelled'){
		$mail_subject = $sale_no.' is not feasible for delivery';
		$mail_content = 'The provider for '.$sale_no.' for '.$account_name.' reported that this service cannot be provisioned. You will find more details on this sales order <a href="'.$sale_url.'">here</a>';
		$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = " . $user_id;
	    }
	    else if ($status == 'Provider Rejected' || $status == 'Batelco Rejected'){
		if($status = 'Provider Rejected'){
			$role_id = 'H21';
			$user_id = '56';
		}
		else{
			$role_id = 'H16';
			$user_id = '19';
		}
	    	$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
				
                $name = 'Event to Contact Customer for provider rejected issues';
                $description = 'This Event was automatically created to contact customer when provider rejected';
                $mail_subject = $sale_no.' is rejected by provider';
		$mail_content = 'The service order request raised for '.$account_name.' for delivery of '.$sale_no.' has been rejected by this provider and an event has been created for you to relay this to the customer.';
            }

	    doLog($process_type);
	    doLog($status);

	    if($name != ''){

		$assigned_to = $entityData['assigned_user_id'];
            	// Assemble the necessary Event fields        
            	$parameters = array(
                    'subject' => $name,
		    'activitytype' => 'Task',
                    'date_start' => $date,
                    'due_date' => $date,
                    'time_start' => '11:00:00',
                    'time_end' => '12:00:00',
		    'eventstatus' => 'Not Started',
		    'visibility' => 'Private',
                    'description' => $description,
                    'duration_hours' => 0,
                    'assigned_user_id' => $assigned_to,
                    'parent_id' => $entityData['id']
            	);
		doLog($parameters);

            	// Create the new Event
            	include_once 'include/Webservices/Create.php';
            	doLog("Creating new Event from SalesOrder for : ".$parameters."...");
            	$resp = vtws_create('Events', $parameters, $current_user);
            	doLog($resp);

		//Send Mails to responsible person
		$event = explode("x", $resp["id"]);
                $event_id = $event[1];
		$event_url = 'http://'.$host_name.'//index.php?module=Calendar&view=Detail&record='.$event_id;
		$mail_content = $mail_content.' You will find more details on this task <a href="'.$event_url.'">here</a>';
	    }

	    if($mail_subject != ''){

		if($name==''){
			$owner_query = 'update vtiger_crmentity set smownerid='.$user_id.' where crmid ='.$saleid;
                	$adb->pquery($owner_query);
                	doLog('update salesorder ownerid to account ownerid');

			$sale_query = 'update vtiger_potential pot, vtiger_salesorder sale set pot.sales_stage = "Closed Lost" where pot.potentialid = sale.potentialid and sale.salesorderid = '.$saleid;
                	$adb->pquery($sale_query);
                	doLog('update salesorder status to Delivered');
		}

		doLog($sel_que);
		$result = $adb->query($sel_que);
		$fields = $result->fields;
		$mail_text = 'Hi '.$fields['first_name'].' '.$fields['last_name'].',<br /><br />'.$mail_content;
	
		$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $fields['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text,'','','');	
            	doLog("--------------------Exit Custom Workflow Function... - create_event_from_salesorder------------------");
	    }
    }
    ?>


