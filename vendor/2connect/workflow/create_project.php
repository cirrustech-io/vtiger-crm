<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

function Project_WorkflowTask_AutoCreateFn($entity) {
 
        global $current_user, $adb, $log;

	$entity_data = $entity->data;

        function doDebug( $data ) {
                /*
                 * Write $data to text log file on filesystem
                 */
                $file = 'logs/workflow_create_project.log';
                file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
                file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );
        } // end function doDebug

        doDebug( $entity_data );
 
        doDebug("Entering Custom Workflow Function: Create Project");
        doDebug("Trigerring Potential is: ".$entity->get('id').".");
	
        //include_once 'include/utils/CommonUtils.php';
        //include_once 'include/database/PearDatabase.php';
 
        // Get the user of the last modifier (or owner) of the entity that triggered this workflow
        $id = explode("x", $entity->get('id'));
        $potid = $id[1];       
       
        doDebug("Querying for user id (".$potid.")...");
        $query = "SELECT smcreatorid, modifiedby FROM vtiger_crmentity WHERE crmid = ?";
        $result = $adb->pquery($query,array($potid));
       
        $modby = $adb->query_result($result,0,"modifiedby");   
        $creby = $adb->query_result($result,0,"smcreatorid");
       
        // If you want the "username" use function getUserName($id)
        if ($modby == 0 ) {
                // Entity not been modified so use creator's ID
                $uname = getUserFullName($creby);
        } else {
                $uname = getUserFullName($modby);
        }
 
        // Build the information for the new Project
        $sub = $entity->get('subject');
        $description = "This Project was automatically created when SalesOrder No. "
                                 . $entity->get('salesorder_no') . " was changed to "
                                 . $entity->get('sostatus') . " by " . $uname . ".\n\n"
                                 . $entity->get('description');

	$name = 'Project in Lead to Order Process for the salesorder ' .$sub;
        $related_to = $entity->get('id');
	$account_id = $entity->get('account_id');
        $assigned_to = $entity->get('assigned_user_id');
	$is_standard = $entity->get('cf_878');
	$salesorder_status = $entity->get('sostatus');
 
        // Just get the current date
	$startdate = date("Y/m/d");
	$targetdate = date("Y/m/d",strtotime("+44 days"));
	
        // Assemble the necessary Project fields        
        $parameters = array(
                'projectname' => $name,
                'startdate' => $startdate,
                'targetenddate' => $targetdate,
                'actualenddate' => $targetdate,
                'description' => $description,
                'projectstatus' => 'initiated',
                'projecttype' => 'operative',
                'progress' => '10%',
                'priority' => 'high',
                'assigned_user_id' => $assigned_to,            
                'targetbudget' => '',
		'salesorder_id' => $related_to,
		'cf_880' => $is_standard,
		'cf_886' => $salesorder_status,
                'linktoaccountscontacts' => $account_id
        );
 
        // Create the new Project
        include_once 'include/Webservices/Create.php';
        doDebug($parameters);
        vtws_create('Project', $parameters, $current_user);
        doDebug("Exit Custom Workflow Function...");
}
?>
