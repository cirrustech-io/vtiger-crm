<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once('vendor/autoload.php');
require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Send_SalesOrder_To_QuickBooks( $entity ) 
{
	global $adb;
	doLog("---------- Start Process - MQ_Send_SalesOrder_To_QuickBooks ----------");
	doLog($GLOBALS['MQ_EXCHANGE_NAME']);

	$routingKey = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_account']['routing_key'];
	$queueName = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_account']['queue_name'];

	// Load Vtiger version number
	global $vtiger_current_version;
	
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_Push_SalesOrder_To_QuickBooks.php',
		'description' => 'Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
	$json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
	$json->_appinfo->mq = new stdClass();
	$json->_appinfo->mq = Array(
		'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
		'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
		'routing_key' => $routingKey,
		'queue' => $queueName,
	);
	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();

	$json->payload->account = Array();
	$json->payload->contact = Array();
	$json->payload->product = Array();
	if(gettype($entity) == 'object'){
		$json->payload->salesorder = $entity->data;
	}
	else{
		doLog($entity);
		$json->payload->salesorder = Vtiger_Record_Model::getInstanceById( $entity, 'SalesOrder' )->getEntity()->column_fields;
	}

	if( $json->payload->salesorder ){

		if(gettype($entity) == 'object'){
                	$product_id = explode('x',$json->payload->salesorder['LineItems'][0]['productid']);
			$prod_id = $product_id[1];
                	$json->payload->salesorder['productid'] = $prod_id;
                	$json->payload->salesorder['quantity'] = $json->payload->salesorder['LineItems'][0]['quantity'];
                	$json->payload->salesorder['listprice'] = $json->payload->salesorder['LineItems'][0]['listprice'];
                	$json->payload->salesorder['record_id'] = $json->payload->salesorder['id'];
                	$contact_id = explode('x',$json->payload->salesorder['contact_id']);
			$con_id = $contact_id[1];
                	$account_id = explode('x',$json->payload->salesorder['account_id']);
			$acc_id = $account_id[1];
			$potential_id = explode('x',$json->payload->salesorder['potential_id']);
                        $pot_id = $potential_id[1];
		}
		else{
			$prod_id = $json->payload->salesorder['productid'];
			$con_id = $json->payload->salesorder['contact_id'];
			$acc_id = $json->payload->salesorder['account_id'];
			$pot_id = $json->payload->salesorder['potential_id'];
			doLog($prod_id);
		}
                // Get Product associated with Sales Order
                $json->payload->product = Vtiger_Record_Model::getInstanceById( $prod_id, 'Products' )->getEntity()->column_fields;
		if (array_key_exists('LineItems', $json->payload->salesorder) == false) {
			$json->payload->salesorder['LineItems'] = Array();
			$line_query = 'select * from vtiger_inventoryproductrel where id ='. $json->payload->salesorder['record_id'];
			$result = $adb->query($line_query);
			foreach ($result as $field){
				array_push($json->payload->salesorder['LineItems'], $field);
			}
			doLog($json->payload->salesorder['LineItems']);
			$json->payload->salesorder['productid'] = $prod_id;
                        $json->payload->salesorder['quantity'] = $json->payload->salesorder['LineItems'][0]['quantity'];
                        $json->payload->salesorder['listprice'] = $json->payload->salesorder['LineItems'][0]['listprice'];
		}		
		
                // Get Contact associated with Sales Order
                $json->payload->contact = Vtiger_Record_Model::getInstanceById( $con_id, 'Contacts' )->getEntity()->column_fields;
		// Get Account associated with Sales Order
                $json->payload->account = Vtiger_Record_Model::getInstanceById( $acc_id, 'Accounts' )->getEntity()->column_fields;
		// Get Potential associated with Sales Order
                $json->payload->potential = Vtiger_Record_Model::getInstanceById( $pot_id, 'Potentials' )->getEntity()->column_fields;
        }

	// Submit message to queue and generate comment for related Opportunity
	if( submitQueue( json_encode( $json ), $routingKey ) ) {
		doLog($routingKey);
		$comment_message = "SalesOrder" . $json->payload->salesorder['salesorder_no'] . " created and submitted to MQ for QuickBooks synchronization.";
	} else {
		$comment_message = "Problem with update SalesOrder " . $json->payload->salesorder['salesorder_no'] . " submission to MQ for QuickBooks synchronization.";
	}
        
	// Create a comment for related Opportunity
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	#$comment->column_fields['related_to'] = $potential_id;
	// $comment->column_fields['userid'] = 5;
	
	//Send Mail to CRM Admin and IT Team
	doLog( json_encode( $json ) );
	$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $GLOBALS['MQ_TO_EMAIL'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $GLOBALS['MQ_MAIL_SUBJECT']['data_transfer_to_qb'], json_encode($json),'','','');

	//Send Email To Customer, cc to AM and CS after Sales Order is Delivered

	$cc_emails = Array();
	$acc_owner_id = $json->payload->salesorder["assigned_user_id"];
	$user_query = 'SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = ' .$acc_owner_id;
        $user_data = $adb->query($user_query);
        $to_email = $adb->query_result($user_data,0,"email1");
	array_push($cc_emails, $to_email);
	
	$role_arr = Array('H16');
	foreach ($role_arr as $role_id){
	    $user_query = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
	    $user_data = $adb->query($user_query);
	    $fields = $user_data->fields;
	    foreach ($fields as $field){
		array_push($cc_emails, $field['email1']);
	    }
	}
	$comma_separated_emailids = implode(",", $cc_emails);
        doLog($comma_separated_emailids);

	$cus_mail = 'abrahambinny@gmail.com';//$json->payload->account["email"];
	$cus_subj = 'CircuitID live and ready for use.';
	$cus_cont = 'Dear Customer, <br/><br/>
			Thank you for choosing 2Connect. Kindly be notified that your below service has been delivered and is ready for use. <br/><br/>
			Service Name : '.$json->payload->salesorder["subject"].' <br/>
			CircuitID    : '.$json->payload->potential["cf_954"].' <br/><br/>
			Please use the CircuitID mentioned above to reference the service when you contact 2Connect for any issues. Our 24/7 helpdesk is available to assist you on 16500110 and <a href="mailto:support@2connect.net">support@2connect.net</a>. <br/><br/><br/>
		      Thanks and Regards, <br/>
	      2Connect Support<b/><b/>';
	$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $cus_mail, $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $cus_subj, $cus_cont,$comma_separated_emailids,'','');
	doLog("---------- End Process - MQ_Send_SalesOrder_To_QuickBooks ----------");
	
	if( $comment->save('ModComments') ) {
		return true;
	} else {
		return false;
	}

}

?>
