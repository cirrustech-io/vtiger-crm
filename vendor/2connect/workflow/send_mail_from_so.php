 <?php
    /*+**********************************************************************************
     * The contents of this file are subject to the vtiger CRM Public License Version 1.0
     * ("License"); You may not use this file except in compliance with the License
     * The Original Code is:  vtiger CRM Open Source
     * The Initial Developer of the Original Code is vtiger.
     * Portions created by vtiger are Copyright (C) vtiger.
     * All Rights Reserved.
     ************************************************************************************/
     
    /**
     * This method is registered with workflow as custom function to be invoked via Task.
     * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
     */

    require_once('config.mq.php');
    require_once('include/utils/utils.php');
    require_once('modules/Emails/mail.php');
    require_once('vendor/2connect/workflow/MQ_Helper.php');
    require_once( 'vendor/autoload.php' );

    function NotificationFromSalesOrder_Workflow_AutoCreateFn($entity) {
     
            global $current_user, $adb, $log;

	    $entityData = $entity->data;
            $entity_id = str_replace("6x","",$entityData['id']);
     
            doLog("---------Entering Custom Workflow Function: Send Notification from (".$entityData['subject'].")... -------");
            doLog("Trigerring SalesOrder is: ".$entity_id.".");

     
            include_once 'include/utils/CommonUtils.php';
            include_once 'include/database/PearDatabase.php';
     
            // Get the saleid
            $id = explode("x", $entityData['id']);
            $saleid = $id[1];
	    $status = $entityData['sostatus'];
	    $process_type = $entityData['cf_884'];
	    $so_no = $entityData['salesorder_no'];
	    
            $date = date("Y/m/d");
	    $mail_subject = '';
	    $mail_content = '';
	    $assigned_to = '';
	    $sel_que = '';
	    $host_name = $GLOBALS['MQ_CLIENT_HOST'];
	    $account_id = explode('x',$entityData['account_id']);
            $acc_id = $account_id[1];
	    $account = Vtiger_Record_Model::getInstanceById( $acc_id, 'Accounts' )->getEntity()->column_fields;
            $acc_name = $account['accountname'];
	    $sale_url = 'http://'.$host_name.'//index.php?module=SalesOrder&view=Detail&record='.$saleid;

            // Build the information for the new Mail
	    if ($status == 'Batelco Rejected' && $process_type == 'Bitstream'){
            	$mail_subject = $so_no. ' of ' .$acc_name. ' rejected by Batelco';
            	$mail_content = 'The Bitstream Order request raised for '.$acc_name.' for delivery of '.$so_no.' is rejected by Batelco. You will find more details on this sales order <a href="'.$sale_url.'">here</a>.';
	    	$assigned_to = $account['assigned_user_id'];
		$sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = " . $assigned_to;
	    }

	    // Build the information for the new Mail
            if ($status == 'Provider Rejected' && $process_type == 'Offnet'){
                $mail_subject = $so_no. ' of ' .$acc_name. ' rejected by Provider';
                $mail_content = 'The Service Order request raised for '.$acc_name.' for delivery of '.$so_no.' is rejected by provider. You will find more details on this sales order <a href="'.$sale_url.'">here</a>.';
                $assigned_to = $account['assigned_user_id'];
                $sel_que = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = " . $assigned_to;
            }

	    doLog($process_type);
	    doLog($status);
	    doLog($assigned_to);

	    if($mail_subject != ''){
		//Send Mails to responsible person
		doLog($sel_que);
                $result = $adb->query($sel_que);
                $fields = $result->fields;

		$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $fields['email1'], $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_content,'','','');	
            	doLog("--------------------Exit Custom Workflow Function... - send_notification_from_salesorder------------------");
	}
    }
    ?>


