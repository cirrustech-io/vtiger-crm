<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function Auto_Provision_AutoCreateFn($entity) {
 
        global $current_user, $adb;
	$entity_data = $entity->data;
        doLog( $entity_data );
 
        doLog("Entering Custom Workflow Function: Auto Provision");
        doLog("Trigerring Project Id : ".$entity_data['id'].".");

	$usr_nme = $entity_data['id'];
	$id = explode("x", $entity_data['id']);
        $entity_id = $id[1];

	function submit_To_Poc_Queue( $json_data ) {
                /*
                 * Received data should be in JSON. This data is submitted to MQ 
                 * TODO: take MQ information (vhost, exchange, routingKey) as parameters to be reusable
                 * TODO: verify that received data is JSON
                 */
                $host = $GLOBALS['MQ_HOST_NAME'];
                $port = $GLOBALS['MQ_PORT'];
                $username = $GLOBALS['MQ_USER_NAME'];
                $password = $GLOBALS['MQ_PASSWORD'];
                $vhost = 'poc_dev';

                $exchange = 'dsl_dev';
                $routingKey = 'ProvisionSubscriber';

                $connection = new AMQPConnection($host, $port, $username, $password, $vhost);
                $channel = $connection->channel();

                #$channel->queue_declare('hello', false, false, false, false);

                $msg = new AMQPMessage($json_data, array('content_type' => 'application/json', 'delivery_mode' => 2));
                $channel->basic_publish($msg, $exchange, $routingKey);

                $channel->close();
                $connection->close();

                return true;
        } // end function submitQueue
	
	$json = new stdClass();
	$provision_data = Array(
                'username' => $usr_nme,
                'package_id' => 707,
                'quota_amount' => 5000,
        );
        $json->payload = $provision_data;
	submit_To_Poc_Queue(json_encode($json));

	//Set the task status to Completed
	$que = 'update vtiger_projecttask set projecttaskstatus = "Completed", projecttaskprogress = "100%" where projecttaskid = '.$entity_id;
	$adb->pquery($que);
	
}
?>
