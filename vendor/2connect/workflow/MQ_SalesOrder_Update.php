<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Update_SalesOrder( $entity ) 
{
	doLog("---------- Start Process - MQ_Salesorder_Update ----------");
        doLog($GLOBALS['MQ_EXCHANGE_NAME']);

        $routingKey = $GLOBALS['MQ_QUEUE_MAP']['workflow_update_salesorder']['routing_key'];
        $queueName = $GLOBALS['MQ_QUEUE_MAP']['workflow_update_salesorder']['queue_name'];
	
	// Load Vtiger version number
	global $vtiger_current_version;
	
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	// $json->_appinfo->application = 'Vtiger CRM 6.1.0';
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_SalesOrder_Update.php',
		'description' => 'Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
        $json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
        $json->_appinfo->mq = new stdClass();
        $json->_appinfo->mq = Array(
                'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
                'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
                'routing_key' => $routingKey,
                'queue' => $queueName,
        );

	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();

	$json->payload->account = Array();
	$json->payload->contact = Array();
	$json->payload->product = Array();
	$json->payload->salesorder = $entity->data;
	
	if( $json->payload->salesorder ){
                // Get Product associated with Sales Order
                $product_id = explode('x',$json->payload->salesorder['LineItems'][0]['productid']);
                $json->payload->salesorder['productid'] = $product_id[1];
                $json->payload->salesorder['quantity'] = $json->payload->salesorder['LineItems'][0]['quantity'];
                $json->payload->salesorder['listprice'] = $json->payload->salesorder['LineItems'][0]['listprice'];
                $json->payload->salesorder['record_id'] = $json->payload->salesorder['id'];
                $json->payload->product = Vtiger_Record_Model::getInstanceById( $product_id[1], 'Products' )->getEntity()->column_fields;
                // Get Contact associated with Sales Order
                $contact_id = explode('x',$json->payload->salesorder['contact_id']);
                $json->payload->contact = Vtiger_Record_Model::getInstanceById( $contact_id[1], 'Contacts' )->getEntity()->column_fields;
		// Get Account associated with Sales Order
                $account_id = explode('x',$json->payload->salesorder['account_id']);
                $json->payload->account = Vtiger_Record_Model::getInstanceById( $account_id[1], 'Accounts' )->getEntity()->column_fields;
                $potential_id = explode('x',$json->payload->salesorder['potential_id']);
		$json->payload->potential = Vtiger_Record_Model::getInstanceById( $potential_id[1], 'Potentials' )->getEntity()->column_fields;
        }

        /*	
	// Get Products
	foreach( $json->payload->invoice['LineItems'] as $key => $value ) {
		// Extract full product entity for each line item
		$pid_array = explode( 'x', $value['productid'] );
		$product_id = $pid_array[1];
		$json->payload->invoice['LineItems'][$key]['product_entity'] = Vtiger_Record_Model::getInstanceById( $product_id, 'Products' )->getEntity()->column_fields;
	}
	
	// Get related Account
	$acctId = explode('x', $json->payload->invoice['salesorder_id']);
	$salesorder = Vtiger_Record_Model::getInstanceById( $acctId[1], 'SalesOrder' )->getEntity()->column_fields;
	$json->payload->salesorder = $salesorder;

	// Get related Contact
	$contactId = explode('x', $json->payload->invoice['contact_id']);
	$contact = Vtiger_Record_Model::getInstanceById( $contactId[1], 'Contacts' )->getEntity()->column_fields;
	$json->payload->contact = $contact;
	
	// Get related Opportunity info to be able to create a comment there
	$salesorder_id = explode('x', $json->payload->invoice['salesorder_id']);
	$salesorder = Vtiger_Record_Model::getInstanceById( $salesorder_id[1], 'SalesOrder' )->getEntity()->column_fields;
	$potential_id = $salesorder['potential_id']; 
        */
	
	// Submit message to queue and generate comment for related Opportunity
	if( submitQueue( json_encode( $json ), $routingKey ) ) {
		$comment_message = "SalesOrder" . $json->payload->salesorder['salesorder_no'] . " created and submitted to MQ for QuickBooks synchronization.";
	} else {
		$comment_message = "Problem with Update SalesOrder " . $json->payload->salesorder['salesorder_no'] . " submission to MQ for QuickBooks synchronization.";
	}
        
	
	// Create a comment for related Opportunity
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	#$comment->column_fields['related_to'] = $potential_id;
	// $comment->column_fields['userid'] = 5;
	
	// Development only
	doLog( json_encode( $json ) );
	doLog("---------- End Process - MQ_Salesorder_Update ----------");
	
	if( $comment->save('ModComments') ) {
		return true;
	} else {
		return false;
	}
	
}

?>
