<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the salesorder and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once( 'vendor/autoload.php' );

function product_checker( $entity ) 
{
       
	global $current_user, $log, $adb;
        $entity_data = $entity->data;

        function doDebug( $data ) {
                /*
                 * Write $data to text log file on filesystem
                 */
                $file = 'logs/workflow_check_for_product_category.log';
                file_put_contents( $file, print_r($data, true), FILE_APPEND | LOCK_EX );
                file_put_contents( $file, "\n", FILE_APPEND | LOCK_EX );
        } // end function doDebug

        doDebug( $entity_data );

	function calculate_date($current_date){

		// Start date and hour		
		$st_date = date_create($current_date);
		date_add($st_date, date_interval_create_from_date_string('1 hours'));
		$start_date = date_format($st_date,'Y-m-d H:i:s');
		$start_hour = date_format($st_date,'H:i');

		//End date and hour
		$ed_date = date_create($current_date);
		date_add($ed_date, date_interval_create_from_date_string('1 days'));
		$end_date = date_format($ed_date,'Y-m-d H:i:s');
		$end_hour = date_format($ed_date,'H:i');

		$date_array = array(
			'start_date' => $start_date,
			'start_hour' => $start_hour,
			'end_date' => $end_date,
			'end_hour' => $end_hour
			);
		return $date_array;	
	}


	function NewActivity_WorkflowTask_AutoCreateFn($entity, $current_user) {
	
   		// Get the information for the New Activity
		doDebug("Here");
   		$subject = 'Discussion about service to deliver';
   		$desc = 'Schedule a meeting between Account Manager and Network Planning Department to discuss about the new service to deliver.';
   		$status = 'Not Started';
   		$priority = 'High';
   		$visibility = 'Public';
   		$activitytype = 'Task';
   		$related_to = $entity->get('id');
   		$process_type = $entity->get('cf_884');
   		$assigned_to = $entity->get('assigned_user_id');
   		$contact_id = $entity->get('contact_id');
		$user_id = '19x'.$current_user->id;
   		$created_time = $entity->get('createdtime');
		$current_date = date();
		$date_array = calculate_date($current_date);
		doDebug($date_array);
		
		$parameters = array( 
			'subject' => $subject,
            		'assigned_user_id' => $assigned_to,
            		'date_start' => $date_array['start_date'],
            		'time_start' => $date_array['start_hour'],
            		'time_end' => $date_array['end_hour'],
            		'due_date' => $date_array['end_date'],
            		'parent_id' => $related_to,
            		'contact_id' => $contact_id,
            		'taskstatus' => $status,
            		'eventstatus' => $status,
            		'taskpriority' => $priority,
            		'sendnotification' => '1',
            		'activitytype' => $activitytype,
            		'visibility' => $visibility,
            		'description' => $desc,
            		'notime' => '0',
            		'duration_hours' => '0',
            		'location' => 'Meeting Room',
            		//'createdtime' => $start_date,
            		//'modifiedtime' => $start_date,
            		//'duration_minutes' => '0',
            		//'reminder_time' => '0',
            		//'recurringtype' => '0',
            		//'modifiedby' => $user_id,
            		//'created_user_id' => $user_id
		);
		
		// Create the new Event
		include_once 'include/Webservices/Create.php';
		doDebug($parameters);
		vtws_create('Events', $parameters, $current_user);
		doDebug("Exit Custom Workflow Function...");

		/*
   		// Get a new Activity Object
   		$newActivity = Activity::getInstance('Activity');

   		// Override the Activity Module parameters with the data from the Sales Order
   		$newActivity->subject = $subject;
   		$newActivity->taskstatus = $status;
   		$newActivity->taskpriority = $priority;
   		$newActivity->visibility = $visibility;
   		$newActivity->activitytype = 'Call';
   		$newActivity->parent_id = $related_to;
   		$newActivity->contact_id = $contact_id;
   		$newActivity->assigned_user_id = $assigned_to;
   		$newActivity->date_start = $start_date;
   		//$newActivity->date_end = $due_date;
		$newActivity->time_start = $time_start;
		$newActivity->time_end = $time_start;
		$newActivity->eventstatus = 'Planned';
		$newActivity->description = $subject;
		$newActivity->duration_hours = '0';
		$newActivity->duration_minutes = '0';
		$newActivity->location = 'NULL';
		$newActivity->notime = '0';
		$newActivity->reminder_time = '0';
		$newActivity->recurringtype = '0';
		$newActivity->sendnotification = '0';
		$newActivity->created_user_id = '19x'.$current_user->id;
   		$newActivity->due_date = $due_date;
   		$newActivity->createdtime = $start_date;
   		$newActivity->modifiedtime = $start_date;
   		$newActivity->modifiedby = '19x'.$current_user->id;
   		$newActivity->ownedby = '19x'.$current_user->id;
		doDebug($newActivity);

  		$newActivity->save('Activity');
		*/

	}


	function find_product_category($entity, $ent_data, $adb, $current_user){
		/*
		 check the product is bitstream or not)
		*/
		
		if($process_type == 'Bitstream'){
			//send notification
		}
		else{
			doDebug("Entering activity creation");
			NewActivity_WorkflowTask_AutoCreateFn($entity, $current_user);
		}
	}

	$prod_array = find_product_category($entity, $entity_data, $adb, $current_user);
	
}

?>
