<?php
/*
 * @title			Workflow Function - MQ_Send_SalesOrder
 * @description		Workflow function triggered on SalesOrder save which submits the potential and related data to MQ.
 */

// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_New_Potential_to_QB( $entity ) 
{
	doLog("---------- Start Process - MQ_Potential_New ----------");
        doLog($GLOBALS['MQ_EXCHANGE_NAME']);

        $routingKey = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_potential']['routing_key'];
        $queueName = $GLOBALS['MQ_QUEUE_MAP']['workflow_new_potential']['queue_name'];

	// Load Vtiger version number
	global $vtiger_current_version;
	
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	// $json->_appinfo->application = 'Vtiger CRM 6.1.0';
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_Potential_Update.php',
		'description' => 'Workflow function triggered on SalesOrder save which submits the potential and related data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
        $json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
        $json->_appinfo->mq = new stdClass();
        $json->_appinfo->mq = Array(
                'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
                'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
                'routing_key' => $routingKey,
                'queue' => $queueName,
        );
	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();

	$json->payload->account = Array();
	$json->payload->contact = Array();
	$json->payload->potential = $entity->data;
	
	if( $json->payload->potential ){
                // Get Contact associated with Sales Order
                $contact_id = explode('x',$json->payload->potential['contact_id']);
                $json->payload->contact = Vtiger_Record_Model::getInstanceById( $contact_id[1], 'Contacts' )->getEntity()->column_fields;
		// Get Account associated with Sales Order
                $account_id = explode('x',$json->payload->potential['related_to']);
                $json->payload->account = Vtiger_Record_Model::getInstanceById( $account_id[1], 'Accounts' )->getEntity()->column_fields;
        }

	
	// Submit message to queue and generate comment for related Opportunity
	if( submitQueue( json_encode( $json ), $routingKey ) ) {
		$comment_message = "Potential" . $json->payload->potential['potential_no'] . " created and submitted to MQ for QuickBooks synchronization.";
	} else {
		$comment_message = "Problem with new Potential " . $json->payload->potential['potential_no'] . " submission to MQ for QuickBooks synchronization.";
	}
        
	
	//  Create a comment for related Opportunity
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	#$comment->column_fields['related_to'] = $potential_id;
	// $comment->column_fields['userid'] = 5;
	
	// Development only
	doLog( json_encode( $json ) );
	doLog("---------- End Process - MQ_Potential_New ----------");
	
	if( $comment->save('ModComments') ) {
		return true;
	} else {
		return false;
	}
	
}

?>
