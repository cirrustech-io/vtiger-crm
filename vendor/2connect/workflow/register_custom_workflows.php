<?php
require_once 'include/utils/utils.php';
require 'modules/com_vtiger_workflow/VTEntityMethodManager.inc';
$emm = new VTEntityMethodManager($adb);
#$emm->addEntityMethod("Accounts", "MQ - Send Accounts", "vendor/2connect/workflow/MQ_Accounts_Update.php", "MQ_Send_Accounts");
#$emm->addEntityMethod("Leads", "Assign to Team Lead", "vendor/2connect/workflow/assign_team_leader.php", "update_lead");
#$emm->addEntityMethod("SalesOrder", "Check for Bitstream", "vendor/2connect/workflow/check_for_product_category.php", "product_checker");
#$emm->addEntityMethod("SalesOrder", "Create Project", "vendor/2connect/workflow/create_project.php", "Project_WorkflowTask_AutoCreateFn");
#$emm->addEntityMethod("ProjectTask", "Create SiteSurvey", "vendor/2connect/workflow/create_sitesurvey.php", "SiteSurvey_WorkflowTask_AutoCreateFn");
#$emm->addEntityMethod("Project", "Create ProjectTasks", "vendor/2connect/workflow/create_projectTask.php", "ProjectTask_WorkflowTask_AutoCreateFn");
#$emm->addEntityMethod("SalesOrder", "Create Batelco Batch", "vendor/2connect/workflow/create_batelco_batch.php", "Batelco_Batch_AutoCreateFn");
#$emm->addEntityMethod("SalesOrder", "Create Project if Batelco Accepted", "vendor/2connect/workflow/create_project_if_batelco_accepted.php", "Project_Batelco_Accepted_AutoCreateFn");
#$emm->addEntityMethod("ProjectTask", "Auto Provision", "vendor/2connect/workflow/auto_provision.php", "Auto_Provision_AutoCreateFn");
#$emm->addEntityMethod("SalesOrder", "Push Information To QuickBooks", "vendor/2connect/workflow/MQ_Push_SalesOrder_To_QuickBooks.php", "MQ_Send_SalesOrder_To_QuickBooks");
#$emm->addEntityMethod("ProjectTask", "Check For Tasks Status", "vendor/2connect/workflow/check_for_tasks_status.php", "Check_Tasks_Status");
#$emm->addEntityMethod("ProjectTask", "Send Notification", "vendor/2connect/workflow/send_crm_mail.php", "Send_Mail_Notification");
#$emm->addEntityMethod("ProjectTask", "Create Event", "vendor/2connect/workflow/create_event_from_task.php", "EventFromTask_Workflow_AutoCreateFn");
#$emm->addEntityMethod("SalesOrder", "Create Event for Approval", "vendor/2connect/workflow/create_event_from_salesorder.php", "EventFromSalesOrder_Workflow_AutoCreateFn");
#$emm->addEntityMethod("Potentials", "Auto Populate", "vendor/2connect/workflow/auto_populate.php", "populate_field");
#$emm->addEntityMethod("SalesOrder", "Auto Populate", "vendor/2connect/workflow/auto_populate.php", "populate_field");
#$emm->addEntityMethod("Potentials", "Auto Update Field", "vendor/2connect/workflow/auto_update_field.php", "update_field");
#$emm->addEntityMethod("Quotes", "Auto Update Field", "vendor/2connect/workflow/update_quote.php", "update_quote_field");
#$emm->addEntityMethod("ProjectTask", "Auto Update SiteSurvey Field", "vendor/2connect/workflow/auto_update_sitesurvey.php", "update_sitesurvey_field");
$emm->addEntityMethod("Products", "Auto Update Product", "vendor/2connect/workflow/auto_update_product.php", "update_product_field");
?>
