<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;


function Check_Tasks_Status($entity) {
 
        global $current_user, $adb, $log;

        doLog("Entering Custom Workflow Function: Check Tasks Status");
        doLog("Trigerring ProjectTask Id : ".$entity->get('id').".");

        // Get the user of the last modifier (or owner) of the entity that triggered this workflow
        $id = explode("x", $entity->get('id'));
        $task_id = $id[1];       

	$pro_id = explode("x", $entity->get('projectid'));
	$project_id = $pro_id[1];
	doLog($project_id);
	$host_name = $GLOBALS['MQ_CLIENT_HOST'];
	$proto_col = $GLOBALS['MQ_PROTOCOL'];
	
	doLog("Querying for project id in task table (".$project_id.")...");
	$query = "SELECT t.projecttaskid, t.projecttaskstatus, t.projecttask_no, tcf.cf_876, p.salesorder_id, p.projectid, p.linktoaccountscontacts, sale.sostatus, scf.cf_884 FROM vtiger_projecttask t, vtiger_projecttaskcf tcf, vtiger_project p, vtiger_salesorder sale, vtiger_salesordercf scf WHERE t.projecttaskid = tcf.projecttaskid AND p.projectid = t.projectid AND p.salesorder_id = scf.salesorderid AND sale.salesorderid = scf.salesorderid AND t.projectid = ? order by tcf.cf_876;";
        $result = $adb->pquery($query,array($project_id));
       
	$task_for = $adb->query_result($result,0,"cf_884");
	$salesorder_id = $adb->query_result($result,0,"salesorder_id");
	$sale_status = $adb->query_result($result,0,"sostatus");
	$acc_id = $adb->query_result($result,0,"linktoaccountscontacts");
	doLog($acc_id);
	
	$task_complete_flag = 'Yes';
	if($task_for == 'Bitstream'){
		$t_status_auto = $adb->query_result($result,0,"projecttaskstatus");
        	$t_status_conf = $adb->query_result($result,1,"projecttaskstatus");
        	$t_status_perf = $adb->query_result($result,2,"projecttaskstatus");
        	$t_status_seti = $adb->query_result($result,3,"projecttaskstatus");
	}
		
	foreach ($result as $res){
		if ($res['projecttaskstatus'] != 'Completed'){
			$task_complete_flag = 'No';
			break;
		}
	}
	
	$acc_query = 'select smownerid from vtiger_crmentity where crmid= ?';
        $acc_owner_data= $adb->pquery($acc_query,array($acc_id));
        $acc_owner_id = $adb->query_result($acc_owner_data,0,"smownerid");

	if(($task_for == 'Bitstream') && ($t_status_conf == 'Completed') && ($t_status_seti == 'Completed') && ($t_status_perf == 'Not Started')){
		doLog($t_status_perf);
		$task_id = $adb->query_result($result,2,"projecttaskid");
		$task_no = $adb->query_result($result,2,"projecttask_no");
		$task_url = $proto_col.'://'.$host_name.'/index.php?module=ProjectTask&view=Detail&record='.$task_id;
		$user_query = 'select u2r.userid, user.email1, user.first_name, user.last_name from vtiger_user2role u2r, vtiger_users user where u2r.userid = user.id and u2r.roleid = "H7";';
		$user_data = $adb->query($user_query);
		$to_email = $adb->query_result($user_data,0,"email1");
		
		$mail_subject = 'Task-'.$task_no.' for ' .$salesorder_id. ' assigned to you.';
                $mail_content = 'Task-'.$task_no.' for installation of ' . $salesorder_id. ' has been assigned to you. You may assign this to a suitable engineer. You will find more details on this task <a href="'.$task_url.'">here</a>';

		$status_query = 'update vtiger_projecttask set projecttaskstatus = "Open" where projecttaskid ='.$task_id;
                $adb->pquery($status_query);
                doLog('update projecttask status to open');

		$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $to_email, $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_content,'','','');
		doLog('Sent mail to ND to perform Installation');
	}
	else if(($sale_status == 'Batelco Delivered' || $sale_status == 'Provider Delivered' || $sale_status == 'In Progress') && ($task_complete_flag == 'Yes')){
		doLog("Initiate updating project and salesorder if all tasks are completed");
                $up_pro_query = 'update vtiger_project set projectstatus="completed", progress="100%" where projectid ='.$project_id;
                $adb->pquery($up_pro_query);
		doLog('Updated project status to completed');

		$up_sale_query = 'update vtiger_salesordercf set cf_892="Open" where salesorderid ='.$salesorder_id;
                $adb->pquery($up_sale_query);
		doLog('update salesorder quickbooks status to Open');

		$sale_query = 'update vtiger_potential pot, vtiger_salesorder sale set pot.sales_stage = "Closed Won", sale.sostatus = "Delivered" where pot.potentialid = sale.potentialid and sale.salesorderid = '.$salesorder_id;
                $adb->pquery($sale_query);
                doLog('update salesorder status to Delivered');

		$owner_query = 'update vtiger_crmentity set smownerid='.$acc_owner_id.' where crmid ='.$salesorder_id;
                $adb->pquery($owner_query);
                doLog('update salesorder ownerid to account ownerid');

		//Send Mail to CS, FN, AM
		$user_query = 'SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = ' .$acc_owner_id;
                $user_data = $adb->query($user_query);
                $to_email = $adb->query_result($user_data,0,"email1");
		$first_name = $adb->query_result($user_data,0,"first_name");
                $last_name = $adb->query_result($user_data,0,"last_name");
		
		$role_arr = Array('H16', 'H18');
		$cc_emails = Array();
		$prj_url = $proto_col.'://'.$host_name.'/index.php?module=Project&relatedModule=ProjectTask&view=Detail&record='.$project_id.'&mode=showRelatedList&tab_label=Project Tasks';

		foreach ($role_arr as $role_id){
		    $user_query = "SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND u2r.roleid = '" . $role_id . "'";
		    $user_data = $adb->query($user_query);
		    $fields = $user_data->fields;
		    foreach ($fields as $field){
		    	array_push($cc_emails, $field['email1']);
		    }
		}

		$comma_separated_emailids = implode(",", $cc_emails);
                doLog($comma_separated_emailids);

		$mail_subject = $salesorder_id.' has been provisioned';
		$mail_content = 'All tasks related to the delivery of '.$salesorder_id.' has now been completed and service is now live. <br/> You may refer to all tasks <a href="'.$prj_url.'">here</a>';
		$mail_text = 'Hi '.$first_name.' '.$last_name.',<br /><br />'.$mail_content;
		$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $to_email, $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text, $comma_separated_emailids,'','');
		doLog('Sent mail to CS, FN, AM after sales order delivery');

		//Push to quickbooks aftesr successfull delivery of sales order
		include_once 'vendor/2connect/workflow/MQ_Push_SalesOrder_To_QuickBooks.php';
		MQ_Send_SalesOrder_To_QuickBooks($salesorder_id);
		doLog('Pushed salesorder data to message queue');

	}
	else if(($sale_status == 'Initiate Planning') && ($task_complete_flag == 'Yes')){

		doLog("Initiate updating project and salesorder if all tasks are completed");
                $up_pro_query = 'update vtiger_project set projectstatus="completed", progress="100%" where projectid ='.$project_id;
                $adb->pquery($up_pro_query);
                doLog('Updated project status to completed');
		
		$prj_url = $proto_col.'://'.$host_name.'/index.php?module=Project&relatedModule=ProjectTask&view=Detail&record='.$project_id.'&mode=showRelatedList&tab_label=Project Tasks';
		$sale_url = $proto_col.'://'.$host_name.'/index.php?module=SalesOrder&view=Detail&record='.$salesorder_id;
		$user_query = 'SELECT usr.id, usr.user_name, usr.first_name, usr.last_name, usr.email1, u2r.roleid FROM vtiger_users usr, vtiger_user2role u2r WHERE usr.id = u2r.userid AND usr.id = ' .$acc_owner_id;
                    $user_data = $adb->query($user_query);
                    $to_email = $adb->query_result($user_data,0,"email1");
		    $first_name = $adb->query_result($user_data,0,"first_name");
		    $last_name = $adb->query_result($user_data,0,"last_name");
                    $mail_subject = 'All Tasks of ' .$salesorder_id.' has been completed';
                    $mail_content = 'All tasks related to the '.$salesorder_id.' has now been completed.<br/> You may refer to all tasks <a href="'.$prj_url.'">here</a> and you can access salesorder <a href="'.$sale_url.'">here</a> and do the remaining procedures.';
		    $mail_text = 'Hi '.$first_name.' '.$last_name.',<br /><br />'.$mail_content;
                    //$status=send_mail($GLOBALS['MQ_MAIL_MODULE']['notify'], $to_email, $GLOBALS['MQ_FROM_NAME'], $GLOBALS['MQ_FROM_EMAIL'], $mail_subject, $mail_text,'','','');
                    doLog('Sent mail to AM after Lead to order completed');

	}

        doLog("Exit Custom Workflow Function...");
}
?>

