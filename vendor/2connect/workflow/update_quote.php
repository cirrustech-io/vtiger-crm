<?php
/*
 * @title		Workflow Function - Auto Populate
 * @description		Workflow function triggered for Auto populating fields
 */

require_once('config.mq.php');
require_once('include/utils/utils.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once('vendor/autoload.php');

function update_quote_field( $entity ) 
{
      
	doLog("-------------------------Entering Custom Workflow Function: Auto Populate Quotes field-----------------------");

	global $log, $adb;
        $entityData = $entity->data;
	doLog($entityData);
	$name = '';
	$que = '';
	$id = explode("x", $entityData['id']);
        $entity_id = $id[1];
	$task_id = explode("x", $entityData['cf_978']);
        $t_id = $task_id[1];
        doLog("Trigerring Entity Id : ".$entity_id);
        doLog($t_id);

	$sel_query = "select sale.accountid, sale.potentialid, sale.contactid from vtiger_projecttask task, vtiger_project prj, vtiger_salesorder sale where task.projectid = prj.projectid and sale.salesorderid = prj.salesorder_id and task.projecttaskid =".$t_id;
	doLog($sel_query);
	$result = $adb->query($sel_query);
        doLog($result);
        $fields = $result->fields;
        $acc_id = $fields['accountid'];
        $pot_id = $fields['potentialid'];

	$que = 'update vtiger_quotes set accountid='.$acc_id.', potentialid='.$pot_id.' where quoteid='.$entity_id;
	doLog($que);
	$adb->pquery($que);
	doLog("-------------------------Exiting Custom Workflow Function: Auto Populate Quotes field--------------------");
	
}

?>
