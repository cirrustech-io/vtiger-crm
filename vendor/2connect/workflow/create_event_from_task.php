 <?php
    /*+**********************************************************************************
     * The contents of this file are subject to the vtiger CRM Public License Version 1.0
     * ("License"); You may not use this file except in compliance with the License
     * The Original Code is:  vtiger CRM Open Source
     * The Initial Developer of the Original Code is vtiger.
     * Portions created by vtiger are Copyright (C) vtiger.
     * All Rights Reserved.
     ************************************************************************************/
     
    /**
     * This method is registered with workflow as custom function to be invoked via Task.
     * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
     */

    require_once('config.mq.php');
    require_once('include/utils/utils.php');
    require_once('modules/Emails/mail.php');
    require_once('vendor/2connect/workflow/MQ_Helper.php');
    require_once( 'vendor/autoload.php' );

    function EventFromTask_Workflow_AutoCreateFn($entity) {
     
            global $current_user, $adb, $log;

	    $entityData = $entity->data;
            $entity_id = str_replace("31x","",$entityData['id']);
     
            doLog("---------Entering Custom Workflow Function: Create Entity from (".$entityData['projecttaskname'].")... -------");
            doLog("Trigerring Task is: ".$entity_id.".");
     
            include_once 'include/utils/CommonUtils.php';
            include_once 'include/database/PearDatabase.php';
     
            // Get the taskid
            $id = explode("x", $entityData['id']);
            $taskid = $id[1];

	    // Get the projectid
            $project = explode("x", $entityData['projectid']);
            $projectid = $project[1];
           
            $log->info("Querying for user id (".$taskid.")...");
            //$query = "SELECT smcreatorid, modifiedby FROM vtiger_crmentity WHERE crmid = ?";
            $query = "SELECT t.projecttaskid FROM vtiger_projecttask t, vtiger_projecttaskcf tcf WHERE t.projectid = ? and tcf.cf_876 = 'Set Price' and t.projecttaskid = tcf.projecttaskid ";
            $result = $adb->pquery($query,array($projectid));
            $price_task_id = $adb->query_result($result,0,"projecttaskid");   
            	    
     
            // Build the information for the new Event
            $name = 'Set Price for the new sales order';
            $description = "This Event was automatically created to discuss about the price settings for the new sales order created which is not a standard product";
            $related_to = $entityData['id'];
	    doLog($related_to);
            $assigned_to = $entityData['assigned_user_id'];
     
            // Just get the current date
            $date = date("Y/m/d");
           
            // Assemble the necessary Project fields        
            $parameters = array(
                    'subject' => $name,
		    'activitytype' => 'Meeting',
                    'date_start' => $date,
                    'due_date' => $date,
                    'time_start' => '10:00:00',
                    'time_end' => '12:00:00',
		    'eventstatus' => 'Planned',
		    'visibility' => 'Private',
                    'description' => $description,
                    'duration_hours' => 0,
                    'assigned_user_id' => 'H17',
                    'parent_id' => '31x'. $price_task_id
            );
     
            // Create the new Event
            include_once 'include/Webservices/Create.php';
            doLog("Creating new Event from Task : ".$parameters."...");
            $resp = vtws_create('Events', $parameters, $current_user);
            doLog($resp);
            doLog("--------------------Exit Custom Workflow Function... - create_event_from_task------------------");
    }
    ?>


