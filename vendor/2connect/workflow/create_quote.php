<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
 
/**
 * This method is registered with workflow as custom function to be invoked via Task.
 * @param $entity Instance of class VTWorkflowEntity will be passed when workflow is being executed.
 */

require_once('include/utils/utils.php');
require_once('modules/Emails/mail.php');
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );

function Quote_WorkflowTask_AutoCreateFn($entity) {
 
        global $current_user, $adb, $log;

	$entity_data = $entity->data;

        doLog( $entity_data );
 
        doLog("Entering Custom Workflow Function: Create Quote");
        doLog("Trigerring Project Task is: ".$entity_data['id'].".");
	
        //include_once 'include/utils/CommonUtils.php';
        //include_once 'include/database/PearDatabase.php';
 
        // Get the user of the last modifier (or owner) of the entity that triggered this workflow
        $id = explode("x", $entity_data['id']);
        $taskid = $id[1];   

	$project_id = explode("x", $entity_data['projectid']);   
	$projectid = $project_id[1];

	doLog($projectid);

	$project = Vtiger_Record_Model::getInstanceById($projectid, 'Project' )->getEntity()->column_fields; 
 	doLog($project);      
        /*$description = "This Quote was automatically created when Project Task No. "
                                 . $entity->get('projecttask_no') . " was changed to "
                                 . $entity->get('projecttaskstatus') . " by " . $uname . ".\n\n"
                                 . $entiity->get('description'); */

	$name = 'Quote for NP';
        $related_to = $entity_data['id'];
        $assigned_to = $entity_data['assigned_user_id'];
 
        // Just get the current date
	$startdate = date("Y/m/d");
	$targetdate = date("Y/m/d",strtotime("+44 days"));
	
        // Assemble the necessary Project fields        
        $parameters = array(
                'name' => $name,
                'description' => $name,
                'assigned_user_id' => $assigned_to,
		'cf_866' => $entity_data['id'],
		'cf_755' => '6x'.$project['salesorder_id']
        );
 
        // Create the new Project
        include_once 'include/Webservices/Create.php';
        doLog($parameters);
        //vtws_create('Quotes', $parameters, $current_user);
        doLog("Exit Custom Workflow Function...");
}
?>
