<?php
/*
 * Workflow Function 
 */


// composer autoloader required for AMQP libraries
require_once('config.mq.php');
require_once('vendor/2connect/workflow/MQ_Helper.php');
require_once( 'vendor/autoload.php' );
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

function MQ_Send_Product( $entity ) 
{
	
	doLog("---------- Start Process - MQ_Products_New ----------");
        doLog($GLOBALS['MQ_EXCHANGE_NAME']);

        $routingKey = $GLOBALS['MQ_QUEUE_MAP']['QB_ProductCreateUpdate']['routing_key'];
        $queueName = $GLOBALS['MQ_QUEUE_MAP']['QB_ProductCreateUpdate']['queue_name'];
	
	global $vtiger_current_version;
		
	// Initialize master json 
	$json = new stdClass();
	$json->_appinfo = new stdClass();
	$json->_appinfo->application = $GLOBALS['MQ_APP_NAME'];
	$json->_appinfo->version = $vtiger_current_version;
	$json->_appinfo->script = Array(
		'name' => 'MQ_Product_CreateUpdate.php',
		'description' => 'Workflow function triggered on Product save which submits the product data to MQ.',
	);
	$json->_appinfo->hostname = $GLOBALS['MQ_CLIENT_HOST'];
        $json->_appinfo->ip_address = $GLOBALS['MQ_CLIENT_IP'];
        $json->_appinfo->mq = new stdClass();
        $json->_appinfo->mq = Array(
                'vhost' => $GLOBALS['MQ_VIRTUAL_HOST'],
                'exchange' => $GLOBALS['MQ_EXCHANGE_NAME'],
                'routing_key' => $routingKey,
                'queue' => $queueName,
        );

	$json->_appinfo->timestamp = date(DATE_ATOM, time());
	$json->payload = new stdClass();
	$json->payload->product = $entity->data;
	
	// Add record_id field to data
	$temp = explode( 'x', $entity->data['id']);
	$json->payload->product['record_id'] = $temp[1];
	
	if( submitQueue( json_encode( $json ), $routingKey ) ) {
		$comment_message = 'MQ_Product_CreateUpdate.php Workflow Function was Triggered which sent a message in to the queue successfully.';
	} else {
		$comment_message = 'ERROR! Problem submitting product data to MQ.';
	}
	
	doLog( json_encode( $json ) );	
	doLog("---------- Start Process - MQ_Products_New ----------");
	
	$comment = CRMEntity::getInstance('ModComments');
	$comment->column_fields['commentcontent'] = $comment_message;
	$comment->column_fields['related_to'] = $potId;
	#$comment->column_fields['userid'] = 5;
	$comment->save('ModComments');
	
	return true;

}

?>

