<?php

require_once("config.php");
include_once 'vtlib/Vtiger/Module.php';

$Vtiger_Utils_Log = true;

$moduleName = 'MODULE NAME'; 

echo "Requested deletion of module: $moduleName";

$module = Vtiger_Module::getInstance( $moduleName );

if ($module) {
	$module->delete();
	echo "Module Deleted!";
} else {
	echo "Module was not deleted.";
}

