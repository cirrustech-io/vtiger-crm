<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

require_once('modules/SalesOrder/views/StandardFormGenerator.php');

class SalesOrder_StandardOrderForm_View extends Vtiger_Export_View {

	function process(Vtiger_Request $request) {

		$viewer = $this->getViewer($request);
    		$saleRecordId = $request->get('record');
        	$recordModel = Vtiger_Record_Model::getInstanceById($saleRecordId, $request->getModule());
		$saleModule = $recordModel->getModule();
	        $salefields = $saleModule->getFields();
		$field_status = $recordModel->get('sostatus');
		$field_process = $recordModel->get('cf_884');
		$field_productid = $recordModel->get('productid');
		$field_productcategory = $recordModel->get('cf_974');
		$field_accountid = $recordModel->get('account_id');
		$field_contactid = $recordModel->get('contact_id');
		$field_potentialid = $recordModel->get('potential_id');
		$field_subject = $recordModel->get('subject');
		$speed = explode('(', $field_subject);
		$prd_speed = '';
		if (count($speed) > 1){
                	$prd_speed = str_replace(")","",$speed[1]);
			$prd_speed = trim(str_replace(" ","",$prd_speed));
		}

        	$accountModel = Vtiger_Record_Model::getInstanceById($field_accountid, 'Accounts');
        	$potentialModel = Vtiger_Record_Model::getInstanceById($field_potentialid, 'Potentials');

		$tech_id = $potentialModel->get('cf_984');
		$bill_id = $potentialModel->get('cf_986');
		$techcontactModel = null;
		$billcontactModel = null;
		if($tech_id!=''){
        		$techcontactModel = Vtiger_Record_Model::getInstanceById($tech_id, 'Contacts');
		}
		if($bill_id!=''){
        		$billcontactModel = Vtiger_Record_Model::getInstanceById($bill_id, 'Contacts');
		}
		
		$db = PearDatabase::getInstance();

		$all_file_path = 'test/upload/orderforms/';
                $pdf_file_name = 'pdforder/standard_orderform_'.$recordModel->get("salesorder_no").'.pdf';
                $html_file_name = 'htmlorder/standard-order-'.$recordModel->get("salesorder_no").'.html';
		
		/*             
		//File Open Code 
		$html_order_file = $all_file_path.'htmlorder/standard-order.html';
		$myfile = fopen($html_order_file, "r") or die("Unable to open file!");
		$html_content = fread($myfile,filesize($html_order_file));
		fclose($myfile);
		*/
	
		$html_order_file = $all_file_path . $html_file_name;
		$html_file = fopen($html_order_file, "w+") or die("Unable to open file!");

		$html_content = generate_standard_form($recordModel, $accountModel, $potentialModel, $techcontactModel, $billcontactModel, $prd_speed, $db);
                fwrite($html_file, $html_content);

                $output_file_path = $all_file_path . $pdf_file_name;
		$type = 'application/pdf';

		$shell_cmd = "wkhtmltopdf '".$html_order_file."' '".$output_file_path."'";
                shell_exec($shell_cmd);
		
		header('Content-Description: File Transfer');
	    	header('Cache-Control: public');
	    	header('Content-Type: '.$type);
	    	header("Content-Transfer-Encoding: binary");
	    	header('Content-Disposition: attachment; filename='. basename($output_file_path));
	    	header('Content-Length: '.filesize($output_file_path));
	    	ob_clean(); #THIS!
	    	flush();
	    	readfile($output_file_path);

	}

}
