<?php

global $RES_PRD_MAP, $BUS_PRD_MAP, $PRD_CAT_MAP, $INTER_PRD_MAP, $DATA_PRD_MAP, $VOICE_PRD_MAP;

$RES_PRD_MAP =  Array('256Kbps/128Kbps'=>'256/128kbps',
             		'512Kbps/256Kbps'=>'512/256kbps',
             		'640Kbps/256Kbps'=>'640/256kbps',
             		'1Mbps/512Kbps'=>'1m/512kbps',
             		'2Mbps/1Mbps'=>'2m/1m',
             		'3Mbps/1Mbps'=>'3m/1m',
             		'4Mbps/1Mbps'=>'4m/1m',
             		'6Mbps/1Mbps'=>'6m/1m',
             		'8Mbps/2Mbps'=>'8m/2m',
             		'10Mbps/2Mbps'=>'10m/2m',
             		'12Mbps/2Mbps'=>'12m/2m',
             		'16Mbps/2Mbps'=>'16m/2m'
			);

$BUS_PRD_MAP =  Array('256Kbps/128Kbps'=>'256/128kbps',
                        '512Kbps/256Kbps'=>'512/256kbps',
                        '640Kbps/256Kbps'=>'640/256kbps',
                        '1Mbps/512Kbps'=>'1m/512kbps',
                        '2Mbps/1Mbps'=>'2m/1m',
                        '4Mbps/1Mbps'=>'4m/1m',
                        '6Mbps/1Mbps'=>'6m/1m',
                        '8Mbps/2Mbps'=>'8m/2m',
                        '10Mbps/2Mbps'=>'10m/2m',
                        '16Mbps/2Mbps'=>'16m/2m'
                	);

$PRD_CAT_MAP = Array('Broadband Internet'=>'',
			'Contended Internet'=>'Contention-Based Internet',
			'CPS'=>'Carrier Pre-Select (CPS)',
			'Dedicated Internet'=>'Dedicated Internet',
			'Hosting'=>'',
			'Internet'=>'',
			'IP Transit'=>'IP Transit',
			'Leased Circuit'=>'',
			'PBX'=>'',
			'Voice Interconnect'=>'',
			'Voice Trunk'=>'Fixed Line Telephony',
			'WDSL'=>'Contention-Based Internet',
			'Web Hosting'=>''
			);

$INTER_PRD_MAP = Array('Contended Internet'=>'Contention-Based Internet',
			'Dedicated Internet'=>'Dedicated Internet',
			'IP Transit'=>'IP Transit'
			);

$DATA_PRD_MAP = Array('Leased Circuit (SDH)'=>'Leased Circuit (SDH)',
			'Leased Circuit (Ethernet)'=>'Leased Circuit (Ethernet)',
			'Leased Circuit (MPLS)' => 'Leased Circuit (MPLS)'
			);

$VOICE_PRD_MAP = Array('CPS'=>'Carrier Pre-Select (CPS)',
			'Voice Trunk'=>'Fixed Line Telephony'
			);






?>
