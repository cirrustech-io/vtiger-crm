<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

require_once('modules/ProjectTask/views/OrderFormGenerator.php');

class ProjectTask_JobOrderForm_View extends Vtiger_Export_View {

	function process(Vtiger_Request $request) {

		/*              
		//File Open Code 
		$html_order_file = 'test/upload/orderforms/job-order.html';
		$myfile = fopen($html_order_file, "r") or die("Unable to open file!");
		$htmlFile = fread($myfile,filesize($html_order_file));
		fclose($myfile);
		*/

		$viewer = $this->getViewer($request);
                $taskRecordId = $request->get('record');
                $recordModel = Vtiger_Record_Model::getInstanceById($taskRecordId, $request->getModule());
                $taskModule = $recordModel->getModule();
                $taskfields = $taskModule->getFields();

		$all_file_path = 'test/upload/orderforms/';
                $pdf_file_name = 'pdforder/job_orderform_'.$recordModel->get("projecttask_no").'.pdf';
                $html_file_name = 'htmlorder/job-order-'.$recordModel->get("projecttask_no").'.html';


		$projectModel = Vtiger_Record_Model::getInstanceById($recordModel->get('projectid'), 'Project');
		$assignuserModel = Vtiger_Record_Model::getInstanceById($recordModel->get('assigned_user_id'), 'Users');
		$saleModel = Vtiger_Record_Model::getInstanceById($projectModel->get('salesorder_id'), 'SalesOrder');
		$potentialModel = Vtiger_Record_Model::getInstanceById($saleModel->get('potential_id'), 'Potentials');
		$accountModel = Vtiger_Record_Model::getInstanceById($saleModel->get('account_id'), 'Accounts');


		$html_order_file = $all_file_path . $html_file_name;
                $html_file = fopen($html_order_file, "w+") or die("Unable to open file!");
                $html_content = generate_order_form($taskRecordId, $recordModel, $accountModel, $potentialModel, $saleModel, $assignuserModel);
                fwrite($html_file, $html_content);
                $output_file_path = $all_file_path . $pdf_file_name;
                $type = 'application/pdf';

                $shell_cmd = "wkhtmltopdf '".$html_order_file."' '".$output_file_path."'";
                shell_exec($shell_cmd);
			
		header('Content-Description: File Transfer');
	    	header('Cache-Control: public');
	    	header('Content-Type: '.$type);
	    	header("Content-Transfer-Encoding: binary");
	    	header('Content-Disposition: attachment; filename='. basename($output_file_path));
	    	header('Content-Length: '.filesize($output_file_path));
	    	ob_clean(); #THIS!
	    	flush();
	    	readfile($output_file_path);

	}

}
