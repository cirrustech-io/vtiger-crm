<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

function generate_order_form($taskRecordId, $recordModel, $accountModel, $potentialModel, $saleModel, $assignuserModel) {

	$html_content = '<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">

<title>Job Order Form</title>

<style type="text/css">

html

{

	font-family: Arial;

	font-size: 10px;

	/* padding: 2em; */

}

body 

{

	font-size: smallest;

}

.textarea

{

	width: 100%;

}

h3

{

	margin-top: 2em;

}





table{

	margin:0px;

	padding:0px;

	border:1px solid #000000;

    border-collapse: collapse;

    border-spacing: 0;

	width:100%;

	/* height:100%; */

}



table td{

	background-color:#ffffff;

	border:1px solid #000000;

	border-width:0px 1px 1px 0px;

	padding:5px;

	color:#000000;

}



table tr:last-child td{

	border-width:0px 1px 0px 0px;

}

table tr td:last-child{

	border-width:0px 0px 1px 0px;

}

table tr:last-child td:last-child{

	border-width:0px 0px 0px 0px;

}

table tr:first-child td{

	background-color:#cccccc;

	border:0px solid #000000;

	border-width:0px 0px 1px 1px;

}

table tr:first-child td:first-child{

	border-width:0px 0px 1px 0px;

}

table tr:first-child td:last-child{

	border-width:0px 0px 1px 1px;

}



#work_requested td

{

	font-size: smaller;

	border: none;

}

#signatures

{

	border: none;

}

#signatures tr td

{

	border-left: none;

	border-right: none;

}

.light_first_child tr:first-child td

{

	background-color: transparent;

}



.gray_bg td

{

	background-color: #CCCCCC;

}



</style>

</head>

<body>

<h2>JOB ORDER</h2>



<!--  Heading Row -->

<h3>1 - Job Details</h3>

<table>

	<!-- Box Title -->

	<tr>

		<td width=14%>Job#</td>

		<td width=14%>TT# or SO#</td>

		<td width=14%>Created Date</td>

		<td width=14%>Created By</td>

		<td width=14%>Job Assigned To</td>

		<td width=14%>Scheduled Job<br/>Date/Time</td>

		<td width=14%>Estimated<br/>Completion Time</td>

	</tr>

	<!-- Box Information -->

	<tr>

		<td>&nbsp;'.$taskRecordId.'</td>

		<td>&nbsp;'.$saleModel->get('salesorder_no').'</td>

		<td>&nbsp;'.$recordModel->get('createdtime').'</td>

		<td>&nbsp;</td>

		<td>&nbsp;'.$assignuserModel->get('first_name').' '.$assignuserModel->get('last_name').'</td>

		<td>&nbsp;'.$recordModel->get('startdate').'</td>

		<td>&nbsp;</td>

	</tr>

	<!-- Box Title -->

	<tr class=gray_bg>

		<td colspan=2>Customer/Company Name</td>

		<td >Circuit ID</td>

		<td colspan=2>Contact Number/Email</td>

		<td colspan=2>Location/Address</td>

	</tr>

	<!-- Box Information -->

	<tr>

		<td colspan=2>&nbsp;'.$accountModel->get('accountname').'</td>

		<td>&nbsp;'.$potentialModel->get('cf_954').'</td>

		<td colspan=2>&nbsp;'.$accountModel->get('phone').' / '.$accountModel->get('email1').'</td>

		<td colspan=2>&nbsp;'.$saleModel->get('cf_918').'</td>

	</tr>

</table>

<br/>

<h4>Task Description</h4>

<!-- Work Details -->

<table class=light_first_child>

        <tr>

                <td>

                        &nbsp;'.$recordModel->get('description').'<br class=textarea />

                        &nbsp;<br/>

                        &nbsp;<br/>

                </td>

        </tr>

</table>


<h4>Work Details</h4>

<!-- Work Details -->

<table class=light_first_child>

	<tr>

		<td>

			&nbsp;<br class=textarea />

			&nbsp;<br/>

			&nbsp;<br/>

		</td>

	</tr>

</table>



<!--  Equipment Details -->

<h3>2 - Equipment Details (if any)</h3>

<table class=light_first_child>

	<tr>

		<td width=30%>Equipment Action: </td>

		<td><input type=checkbox /> Deploy</td>

		<td><input type=checkbox /> Recover</td>

	</tr>

</table>

<br/>

<table>

	<tr>

		<td width=10%>No.</td>

		<td width=20%>Part Number</td>

		<td width=40%>Description</td>

		<td width=10%>Quantity</td>

		<td width=20%>Serial Number</td>

	</tr>

	<!-- Repeat for multiple line items -->

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

</table>



<!--  Implementation Details -->

<h3>3 - Implementations Details</h3>

<table>

	<tr>

		<td width=20%>Work Date Date</td>

		<td width=20%>Vehicle</td>

		<td width=20%>Travel Time</td>

		<td width=20%>Arrived on Site</td>

		<td width=20%>Departed Site</td>

	</tr>

	<!-- Repeat for multiple line items -->

	<tr>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

		<td>&nbsp;</td>

	</tr>

	<!-- Engineer Technician Remarks -->

	<tr>

		<td colspan=5>

			<strong>Engineer Technician Remarks</strong><br/>

		</td>

	</tr>

	<tr>

		<td colspan=5 border=1>

			&nbsp;<br/>

			&nbsp;<br/>

			&nbsp;<br/>

		</td>

	</tr>

</table>



<br/>

<br/>

<!-- Signatures -->

<p>By signing the below, I acknowledge the attendance of the engineer/technician to perform the job and the deployment (or recovery) of the items as per the above mentioned details.</p>

<table id=signatures class=light_first_child>

	<tr>

		<td width=50%>Customer Name: </td>

		<td width=20%>Date: </td>

		<td width=30%>Signature: </td>

	</tr>

	<tr>

		<td colspan=3>Comments: </td>

	</tr>

	<tr>

		<td>Engineer Name: </td>

		<td>Date: </td>

		<td>Signature: </td>

	</tr>

	<tr>

		<td>Supervisor Name: </td>

		<td>Date: </td>

		<td>Signature: </td>

	</tr>

	<tr>

		<td colspan=3>Comments: </td>

	</tr>

</table>



</body>

</html>';
	return $html_content;
	
}
