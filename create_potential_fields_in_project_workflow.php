<?php // Turn on debugging level
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('Project');
$block1 = Vtiger_Block::getInstance('LBL_PROJECT_INFORMATION',$module);

$field1 = new Vtiger_Field();
$field1->label = 'Opportunity';
$field1->name = 'potential_id';
$field1->table = 'vtiger_project';
$field1->column = 'potential_id';
$field1->columntype = 'VARCHAR(10)';
$field1->uitype = 10;
$field1->typeofdata = 'V~O';

$block1->addField($field1);
$field1->setRelatedModules(Array('Potentials'));

$block1->save($module);
?>
