<?php

require_once ('lib/swift_required.php');

// Create the message
$message = Swift_Message::newInstance();
$message -> setSubject('Test Swift Mail');
$message -> setFrom(array('crm@i.2connectbahrain.com' => '2Connect Support'));
$message -> setTo(array('binny.abraham@2connectbahrain.com'));
$message -> setBody('Here is the message itself');
$message -> attach(Swift_Attachment::fromPath('batchfiles/B-BTS-09.02.2015-2.pdf'));

//send the message          
$mailer->send($message);

?>
