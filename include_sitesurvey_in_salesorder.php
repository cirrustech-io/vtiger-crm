<?php

require_once 'vtlib/Vtiger/Module.php';
require_once 'vtlib/Vtiger/Package.php';

$module = Vtiger_Module::getInstance('SalesOrder');
$storemodule1 = Vtiger_Module::getInstance('SiteSurveys');
$relationLabel = 'Site Surveys';
$function_name = 'get_sitesurveys';
$module->setRelatedList( $storemodule1, $relationLabel, Array('ADD','SELECT'), $function_name );
