<?php // Turn on debugging level
$Vtiger_Utils_Log = true;
include_once('vtlib/Vtiger/Menu.php');
include_once('vtlib/Vtiger/Module.php');

$module = Vtiger_Module::getInstance('SiteSurveys');
$mymodule = Vtiger_Module::getInstance('ProjectTask');
$block1 = Vtiger_Block::getInstance('LBL_SITESURVEY_INFORMATION',$module);

$field1 = new Vtiger_Field();
$field1->label = 'Project Task';
$field1->name = 'projecttask';
$field1->table = 'vtiger_sitesurveys';
$field1->column = 'projecttask_id';
$field1->columntype = 'VARCHAR(10)';
$field1->uitype = 10;
$field1->typeofdata = 'V~O';

$block1->addField($field1);
$field1->setRelatedModules(Array('ProjectTask'));

$block1->save($module);
?>
