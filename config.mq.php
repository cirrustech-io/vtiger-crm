<?php
/*
 * @title              	Workflow Function - MQ_Config
 * @description         It's a MQ Config file
 */
// composer autoloader required for AMQP libraries

global $MQ_HOST_NAME, $MQ_PORT, $MQ_USER_NAME, $MQ_PASSWORD, $MQ_VIRTUAL_HOST, $MQ_EXCHANGE_NAME, $MQ_QUEUE_MAP;
global $MQ_FROM_EMAIL, $MQ_TO_EMAIL, $MQ_CC_EMAIL, $MQ_BCC_EMAIL, $MQ_FROM_NAME, $MQ_MAIL_MODULE, $MQ_MAIL_SUBJECT;
global $MQ_APP_NAME, $MQ_CLIENT_HOST, $MQ_CLIENT_IP, $MQ_LOG_FILE_PATH;

//MQ_Credentials
$MQ_HOST_NAME = 'mq.2connectbahrain.com';
$MQ_PORT = 5672;
$MQ_USER_NAME = 'guest';
$MQ_PASSWORD = 'guest';
$MQ_VIRTUAL_HOST = 'crm_dev';
$MQ_EXCHANGE_NAME = 'cqb_dev';
$MQ_CONTENT_TYPE = 'application/json';
$MQ_DELIVERY_MODE = 2;

//MQ_Queue
$MQ_QUEUE_MAP = Array('workflow_new_account'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.new_account'),
             'workflow_update_account'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.update_account'),
             'qb_new_account'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.new_account'),
             'qb_update_account'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.update_account'),

             'workflow_new_potential'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.new_potential'),
             'workflow_update_potential'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.update_potential'),
             'qb_new_potential'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.new_potential'),
             'qb_update_potential'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.update_potential'),

             'workflow_new_salesorder'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.new_salesorder'),
             'workflow_update_salesorder'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.update_salesorder'),
             'qb_new_salesorder'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.new_salesorder'),
             'qb_update_salesorder'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.update_salesorder'),

             'workflow_new_product'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.new_product'),
             'workflow_update_product'=>Array('queue_name'=>'UpdateQB','type'=>'direct', 'routing_key'=>'workflow.update_product'),
             'QB_ProductCreateUpdate'=>Array('queue_name'=>'QB_ProductCreateUpdate','type'=>'direct', 'routing_key'=>'workflow.product.CreateUpdate'),
             'qb_new_product'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.new_product'),
             'qb_update_product'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.update_product'),

             'NewInvoice'=>Array('queue_name'=>'NewInvoice','type'=>'direct', 'routing_key'=>'workflow.new_invoice'),
             'qb_new_invoice'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.new_invoice'),
             'qb_update_invoice'=>Array('queue_name'=>'UpdateCRM','type'=>'direct', 'routing_key'=>'qb.update_invoice'),
             'InvoicePDF'=>Array('queue_name'=>'InvoicePDF','type'=>'direct', 'routing_key'=>'crm.pdf_invoice'),
             'InvoiceEmail'=>Array('queue_name'=>'InvoiceEmail','type'=>'direct', 'routing_key'=>'crm.email_invoice'),

             'UpdateSuccess'=>Array('queue_name'=>'UpdateSuccess','type'=>'direct', 'routing_key'=>'#.update_crm_success'),
             'QB_log'=>Array('queue_name'=>'QB_log','type'=>'direct', 'routing_key'=>'QB_log'),
             );

//MQ_Mailer_Credentials
$MQ_FROM_EMAIL = 'crm@i.2connect.net';
$MQ_TO_EMAIL = 'binny.abraham@2connect.net';
$MQ_CC_EMAIL = 'binny.abraham@2connect.net';
$MQ_BCC_EMAIL = '';
$MQ_FROM_NAME = '2Connect CRM';
$MQ_MAIL_MODULE = array("notify"=>"Notify", "attach"=>"Attach");
$MQ_MAIL_SUBJECT = array("data_transfer_to_qb"=>"Data transfered to Quick Books", "invoice"=>"Your Invoice Attached");

//MQ_Client_Details
$MQ_APP_NAME = 'Vtiger CRM';
$MQ_CLIENT_HOST = 'dev-crm.i.2connect.net';
$MQ_CLIENT_IP = '192.168.2.95';
$MQ_LOG_FILE_PATH = 'logs/workflow_MQ_CRM_To_QB.log';


?>


