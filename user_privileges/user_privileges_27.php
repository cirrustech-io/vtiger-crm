<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H4';

$current_user_parent_role_seq='H1::H2::H3::H4';

$current_user_profiles=array(12,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'2'=>0,'4'=>0,'6'=>0,'7'=>0,'8'=>0,'9'=>0,'10'=>1,'13'=>1,'14'=>0,'15'=>1,'16'=>0,'18'=>0,'19'=>1,'20'=>1,'21'=>1,'22'=>0,'23'=>0,'24'=>1,'25'=>0,'26'=>0,'27'=>1,'30'=>1,'31'=>1,'35'=>1,'37'=>1,'38'=>1,'39'=>1,'40'=>1,'42'=>0,'43'=>1,'44'=>1,'45'=>0,'46'=>0,'47'=>0,'48'=>1,'49'=>1,'50'=>0,'28'=>1,'3'=>0,);

$profileActionPermission=array(2=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),4=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,8=>1,10=>1,),6=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,8=>1,10=>1,),7=>array(0=>0,1=>0,2=>0,4=>0,5=>1,6=>1,8=>1,9=>1,10=>1,),8=>array(0=>0,1=>0,2=>0,4=>0,6=>1,),9=>array(0=>0,1=>0,2=>0,4=>0,5=>1,6=>1,),13=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,8=>1,10=>1,),14=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),15=>array(0=>1,1=>1,2=>1,4=>0,),16=>array(0=>0,1=>0,2=>0,4=>0,5=>1,6=>1,),18=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),19=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),20=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,),21=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,),22=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,),23=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>1,13=>1,),26=>array(0=>1,1=>1,2=>1,4=>0,),30=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),31=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),37=>array(0=>1,1=>1,2=>1,4=>0,11=>1,12=>1,),40=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),42=>array(0=>1,1=>1,2=>1,4=>0,),43=>array(0=>1,1=>1,2=>1,4=>0,),45=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),46=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),47=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,10=>1,),50=>array(0=>1,1=>1,2=>1,4=>0,5=>1,6=>1,8=>1,),);

$current_user_groups=array(2,3,4,);

$subordinate_roles=array('H16','H5',);

$parent_roles=array('H1','H2','H3',);

$subordinate_roles_users=array('H16'=>array(40,41,),'H5'=>array(25,26,28,29,30,60,),);

$user_info=array('user_name'=>'ahmed.isa','is_admin'=>'off','user_password'=>'$1$ah000000$xv.FMczlnFIG/kinXPngj.','confirm_password'=>'$1$ah000000$xv.FMczlnFIG/kinXPngj.','first_name'=>'Ahmed','last_name'=>'Isa','roleid'=>'H4','email1'=>'binny.abraham@2connect.net','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'12','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'yBsFzFSr8jB0GrS','time_zone'=>'Asia/Kuwait','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>',','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'en_us','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'medium','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','currency_name'=>'Bahrain, Dinar','currency_code'=>'BHD','currency_symbol'=>'BD','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'27');
?>