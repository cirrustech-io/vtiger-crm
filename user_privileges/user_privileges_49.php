<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H11';

$current_user_parent_role_seq='H1::H2::H3::H20::H8::H11';

$current_user_profiles=array(10,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'2'=>0,'4'=>0,'6'=>0,'7'=>0,'8'=>0,'9'=>0,'10'=>1,'13'=>0,'14'=>0,'15'=>1,'16'=>0,'18'=>0,'19'=>1,'20'=>1,'21'=>1,'22'=>0,'23'=>0,'24'=>1,'25'=>1,'26'=>0,'27'=>1,'30'=>1,'31'=>1,'35'=>1,'37'=>1,'38'=>1,'39'=>1,'40'=>1,'42'=>0,'43'=>1,'44'=>1,'45'=>0,'46'=>0,'47'=>0,'48'=>1,'49'=>1,'50'=>0,'28'=>1,'3'=>0,);

$profileActionPermission=array(2=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,10=>0,),4=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,8=>1,10=>0,),6=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,8=>1,10=>0,),7=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,8=>1,9=>0,10=>0,),8=>array(0=>0,1=>0,2=>1,4=>0,6=>0,),9=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,),13=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,8=>1,10=>0,),14=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,10=>0,),15=>array(0=>1,1=>1,2=>1,4=>0,),16=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,),18=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,10=>0,),19=>array(0=>1,1=>1,2=>1,4=>1,5=>1,6=>1,10=>0,),20=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),21=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),22=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,),23=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>0,13=>0,),26=>array(0=>1,1=>1,2=>1,4=>0,),30=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,10=>0,),31=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,10=>0,),37=>array(0=>1,1=>1,2=>1,4=>1,11=>0,12=>0,),40=>array(0=>1,1=>1,2=>1,4=>1,5=>1,6=>1,10=>0,),42=>array(0=>1,1=>1,2=>1,4=>0,),43=>array(0=>0,1=>0,2=>0,4=>0,),45=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,10=>0,),46=>array(0=>0,1=>0,2=>1,4=>0,5=>0,6=>0,10=>0,),47=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,10=>0,),50=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,8=>1,),);

$current_user_groups=array(17,3,4,);

$subordinate_roles=array();

$parent_roles=array('H1','H2','H3','H20','H8',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'taheer.aga','is_admin'=>'off','user_password'=>'$1$ta000000$RjqP54K.mrgtuTIQG9gav.','confirm_password'=>'$1$ta000000$RjqP54K.mrgtuTIQG9gav.','first_name'=>'Taheer','last_name'=>'Aga','roleid'=>'H11','email1'=>'binny.abraham@2connect.net','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'12','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'q2qLbD98dU1wLWiK','time_zone'=>'Asia/Kuwait','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>',','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'en_us','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'medium','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','currency_name'=>'Bahrain, Dinar','currency_code'=>'BHD','currency_symbol'=>'BD','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'49');
?>