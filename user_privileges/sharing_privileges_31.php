<?php


//This is the sharing access privilege file
$defaultOrgSharingPermission=array('2'=>0,'4'=>0,'6'=>0,'7'=>0,'9'=>3,'13'=>0,'16'=>3,'20'=>3,'21'=>0,'22'=>0,'23'=>0,'26'=>0,'8'=>0,'14'=>0,'30'=>0,'31'=>0,'37'=>0,'40'=>0,'42'=>0,'43'=>0,'45'=>0,'46'=>0,'47'=>0,'18'=>0,'10'=>0,'50'=>0,);

$related_module_share=array(2=>array(6,),13=>array(6,),20=>array(6,2,),22=>array(6,2,20,),23=>array(6,22,),);

$Leads_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Documents_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Documents_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Products_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Products_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Vendors_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Vendors_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Services_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Services_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ServiceContracts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ServiceContracts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$PBXManager_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$PBXManager_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Assets_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Assets_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ModComments_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ModComments_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ProjectMilestone_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ProjectMilestone_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ProjectTask_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$ProjectTask_share_write_permission=array('ROLE'=>array(),'GROUP'=>array(15=>array(0=>37,1=>38,2=>36,),17=>array(0=>46,1=>47,2=>48,3=>49,4=>50,5=>45,),));

$Project_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Project_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SiteSurveys_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SiteSurveys_share_write_permission=array('ROLE'=>array(),'GROUP'=>array(15=>array(0=>37,1=>38,2=>36,),));

?>